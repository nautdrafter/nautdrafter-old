/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.lobby.network;

import com.monkygames.nautdrafter.common.Constants;
import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.Server;
import com.monkygames.nautdrafter.common.network.ServerInfo;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.UserDetailsPacket;
import com.monkygames.nautdrafter.lobby.network.callbacks.ClientIdentityCallback;
import com.monkygames.nautdrafter.lobby.network.callbacks.LobbyCallbacks;
import com.monkygames.nautdrafter.lobby.network.callbacks.ServerAnnounceCallback;
import com.monkygames.nautdrafter.lobby.network.callbacks.ServerListRequestCallback;
import com.monkygames.nautdrafter.lobby.network.packets.AuthoriseNewRequestPacket;
import com.monkygames.nautdrafter.lobby.network.packets.ClientIdPacket;
import com.monkygames.nautdrafter.lobby.network.packets.OpenIdHttpGetRequestPacket;
import com.monkygames.nautdrafter.lobby.network.packets.ServerAnnouncePacket;
import com.monkygames.nautdrafter.lobby.network.packets.ServerListPacket;
import com.monkygames.nautdrafter.lobby.network.packets.ServerListRequestPacket;
import com.monkygames.nautdrafter.lobby.network.packets.ServerUpdatePacket;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.util.Pair;

public class LobbyServer extends Server implements ServerAnnounceCallback, ServerListRequestCallback, ClientIdentityCallback {

    private final Map<Pair<InetAddress, Integer>, DraftingConnectionInfo> drafting_servers = new HashMap<>();
    private final Map<SelectionKey, Pair<InetAddress, Integer>> drafting_server_keys = new HashMap<>();

    private final Map<UUID, SelectionKey> pending_auths = new HashMap<>();
    private final Map<SelectionKey, UUID> pending_auth_keys = new HashMap<>();

    public LobbyServer(InetSocketAddress addr) throws IOException {
        super(addr);
        LobbyCallbacks.serverAnnounceCallback = this;
        LobbyCallbacks.serverListRequestCallback = this;
        LobbyCallbacks.clientIdentityCallback = this;
    }

    @Override
    protected void kill(SelectionKey key) {
        super.kill(key);
        // if it was a drafting server that disconnected, remove it
        Pair<InetAddress, Integer> location = this.drafting_server_keys.remove(key);
        if (location != null) {
            DraftingConnectionInfo info = drafting_servers.remove(location);
            if (info != null) {
                Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Removed server: {0}@{1}:{2}", new Object[]{info.serverInfo.name, info.serverInfo.address, info.serverInfo.port});
            }
        }
        // if it was a pending client auth, remove it
        UUID clientId = this.pending_auth_keys.remove(key);
        if (clientId != null) {
            this.pending_auths.remove(clientId);
        }
    }

    @Override
    protected Packet createPacket(SelectionKey key, byte[] data) throws IOException {
        // ConnectionInfo info = (ConnectionInfo) key.attachment();
        switch (data[0]) {
        case Packet.SERVER_ANNOUNCE_PACKET_ID:
            return new ServerAnnouncePacket(data, key);
        case Packet.SERVER_UPDATE_PACKET_ID:
            return new ServerUpdatePacket(data, key);
        case Packet.SERVER_LIST_REQUEST_PACKET_ID:
            return new ServerListRequestPacket(data, key);
        case Packet.HTTP_GET_REQUEST_ID:
            return new OpenIdHttpGetRequestPacket(data);
        case Packet.CLIENTID_PACKET_ID:
            return new ClientIdPacket(data, key);
        case Packet.NEWAUTH_PACKET_ID:
            return new AuthoriseNewRequestPacket(data, key);
        }
        return null;
    }

    @Override
    public void onServerAnnounce(ServerInfo serverInfo, SelectionKey key) {
        DraftingConnectionInfo info = new DraftingConnectionInfo((ConnectionInfo) key.attachment(), key, serverInfo);
        key.attach(info);
        Pair<InetAddress, Integer> location = new Pair(serverInfo.address, serverInfo.port);
        drafting_servers.put(location, info);
        drafting_server_keys.put(key, location);
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Server Announce: {0}@{1}:{2} - {3} players", new Object[]{serverInfo.name, serverInfo.address, serverInfo.port, serverInfo.player_count});
    }

    @Override
    public void onServerUpdate(int playerCount, SelectionKey key) {
        ServerInfo serverInfo = ((DraftingConnectionInfo) key.attachment()).serverInfo;
        DraftingConnectionInfo info = this.drafting_servers.get(this.drafting_server_keys.get(key));
        info.serverInfo.player_count = playerCount;
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Server Update: {0}@{1}:{2} - {3} players", new Object[]{serverInfo.name, serverInfo.address, serverInfo.port, serverInfo.player_count});
    }

    @Override
    public void onServerListRequest(SelectionKey key) {
        try {
            ConnectionInfo info = (ConnectionInfo) key.attachment();
            ServerListPacket packet = new ServerListPacket(this.drafting_servers.values());
            info.send_buffer.put(packet.data);
            this.write(key);
        } catch (PacketEncoderException ex) {
            Logger.getLogger(LobbyServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void onClientUnknown(UUID clientId, SelectionKey key) {
        this.pending_auths.put(clientId, key);
        this.pending_auth_keys.put(key, clientId);
    }

    @Override
    public boolean onClientIdentify(UUID clientId, Long steamId, SelectionKey key) {
        if (key == null) {
            key = this.pending_auths.get(clientId);
        }
        if (key == null) {
            return false;
        }
        UserDetailsPacket packet = SteamAPI.getDetails(clientId, steamId);
        Player player = packet.getPlayer();
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Sending user details for {0} ({1})", new Object[]{player.name, player.steamId});
        ((ConnectionInfo) key.attachment()).send_buffer.put(packet.data);
        this.write(key);
        return true;
    }

    /**
     * The main loop.
     *
     * @param args address and port to bind to, if specified (defaults to the default local address, and port 8080)
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        InetAddress addr = null;
        int port = Constants.LOBBY_PORT;
        if (args.length > 0) {
            addr = InetAddress.getByName(args[0]);
        }
        if (args.length > 1) {
            port = Integer.parseInt(args[1]);
        }
        SteamAPI.touch();
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Loading database...");
        UserDetails.touch();
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Database loaded!");
        new LobbyServer(new InetSocketAddress(addr, port)).run();
    }
}
