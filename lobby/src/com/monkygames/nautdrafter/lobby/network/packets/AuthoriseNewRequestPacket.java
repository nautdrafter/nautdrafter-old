/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.lobby.network.callbacks.LobbyCallbacks;
import java.nio.channels.SelectionKey;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This packet is used to tell the client to authorise a new client
 */
public class AuthoriseNewRequestPacket extends Packet {

    /**
     * The constant used for the specific packet ID
     */
    public static final int PACKET_ID = NEWAUTH_PACKET_ID;

    private final SelectionKey key;

    /**
     * Constructor taking a byte[] and creating a new packet form this
     * @param data 
     * @param key
     */
    public AuthoriseNewRequestPacket(byte[] data, SelectionKey key) {
        super(data);
        this.key = key;
    }

    /**
     * How to decode this packet
     * @return
     * @throws PacketDecoderException 
     */
    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        return true;
    }

    /**
     * What you receive this packet, create a new ClientID Packet
     */
    @Override
    protected void handle_impl() {
        UUID clientId = UUID.randomUUID();
        try {
            this.response = new ClientIdPacket(clientId.toString());
        } catch (PacketEncoderException ex) {
            Logger.getLogger(AuthoriseNewRequestPacket.class.getName()).log(Level.SEVERE, null, ex);
            this.disconnect = true;
        }
        if (LobbyCallbacks.clientIdentityCallback != null) {
            LobbyCallbacks.clientIdentityCallback.onClientUnknown(clientId, this.key);
        }
    }
}