/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.lobby.network.callbacks.LobbyCallbacks;
import java.nio.channels.SelectionKey;

/**
 * Requests the server list
 */
public class ServerListRequestPacket extends Packet {

    public static final int PACKET_ID = SERVER_LIST_REQUEST_PACKET_ID;
    private SelectionKey key;

    public ServerListRequestPacket(byte[] data, SelectionKey key) {
        super(data);
        this.key = key;
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        return true;
    }

    @Override
    protected void handle_impl() {
        if (LobbyCallbacks.serverListRequestCallback != null) {
            LobbyCallbacks.serverListRequestCallback.onServerListRequest(this.key);
        }
        this.disconnect = true;
    }

}
