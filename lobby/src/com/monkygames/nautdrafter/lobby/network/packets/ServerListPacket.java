/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.lobby.network.DraftingConnectionInfo;
import com.monkygames.nautdrafter.common.network.packets.segments.ServerInfoSegment;
import java.util.Collection;

/**
 * A list of servers
 */
public class ServerListPacket extends Packet {

    public static final int PACKET_ID = SERVER_LIST_PACKET_ID;

    public ServerListPacket(Collection<DraftingConnectionInfo> servers) throws PacketEncoderException {
        int server_count = servers.size();
        if (server_count > 0xFFFF) {
            throw new PacketEncoderException("Too many servers!");
        }
        byte[][] buffers = new byte[server_count][];
        int i = 0;
        int length = 0;
        for (DraftingConnectionInfo server : servers) {
            byte[] server_buffer = ServerInfoSegment.encode(server.serverInfo);
            length += server_buffer.length;
            buffers[i++] = server_buffer;
        }
        this.data = new byte[1 + 2 + length];
        int pos = 0;
        this.data[pos++] = PACKET_ID;
        this.data[pos++] = (byte) ((server_count >> 8) & 0xFF);
        this.data[pos++] = (byte) ((server_count) & 0xFF);
        for (byte[] buf : buffers) {
            System.arraycopy(buf, 0, this.data, pos, buf.length);
            pos += buf.length;
        }
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    protected void handle_impl() {
        throw new UnsupportedOperationException("Not supported");
    }

}
