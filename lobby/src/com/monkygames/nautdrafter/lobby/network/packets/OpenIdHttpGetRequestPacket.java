/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.Constants;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.lobby.network.callbacks.LobbyCallbacks;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Attempts to read the first line of a HTTP GET request to retrieve OpenID auth
 */
public class OpenIdHttpGetRequestPacket extends Packet {

    private static final byte G = 71;
    private static final byte E = 69;
    private static final byte T = 84;
    private static final byte SPACE = 32;
    private static final byte LINE_FEED = 10;

    private boolean checked_get = false;
    private int last_space = 4;
    private String request;

    public OpenIdHttpGetRequestPacket(byte[] data) {
        super(data);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        if (this.data.length < this.last_space) {
            return false;
        }
        if (!checked_get) {
            if (this.data[0] != G || this.data[1] != E || this.data[2] != T || this.data[3] != SPACE) {
                throw new PacketDecoderException("Wasn't a valid HTTP GET request");
            }
            this.checked_get = true;
        }
        int max = this.max();
        for (; this.decode_from < max; this.decode_from++) {
            if (this.data[this.decode_from] == SPACE) {
                this.last_space = this.decode_from;
            }
            if (this.data[this.decode_from] == LINE_FEED) {
                this.request = new String(this.data, 4, this.last_space - 4, Packet.charset);
                this.pos = this.decode_from;
                return true;
            }
        }
        this.pos = this.decode_from;
        return false;
    }

    @Override
    protected void handle_impl() {
        String[] splitArray = this.request.split("\\?", 2);
        String userID = splitArray[0].substring(1);
        splitArray = splitArray[1].split("http%3A%2F%2Fsteamcommunity.com%2Fopenid%2Fid%2F", 2);
        splitArray = splitArray[1].split("&openid.identity", 2);
        String steamID = splitArray[0];
        Logger.getLogger(OpenIdHttpGetRequestPacket.class.getName()).log(Level.INFO, "Identified {0} as {1}", new Object[]{userID, steamID});
        if (LobbyCallbacks.clientIdentityCallback != null && LobbyCallbacks.clientIdentityCallback.onClientIdentify(UUID.fromString(userID), Long.parseLong(steamID), null)) {
            this.response = new HttpRedirectPacket(Constants.SUCCESS_PAGE);
        } else {
            this.response = new HttpRedirectPacket(Constants.FAILURE_PAGE);
        }
        this.disconnect = true;
    }
}
