/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.network.ServerInfo;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.ServerAnnouncePacketEncoder;
import com.monkygames.nautdrafter.common.network.packets.segments.ServerInfoSegment;
import com.monkygames.nautdrafter.lobby.network.callbacks.LobbyCallbacks;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;

/**
 * Lobby Server implementation of ServerAnnouncePacket
 */
public class ServerAnnouncePacket extends ServerAnnouncePacketEncoder {

    private final SelectionKey key;

    public ServerAnnouncePacket(byte[] data, SelectionKey key) throws PacketDecoderException {
        super(data);
        this.key = key;
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        // name
        if (this.serverInfo == null) {
            if (!ServerInfoSegment.can_decode(this, false)) {
                return false;
            }
            this.serverInfo = ServerInfoSegment.decode(this, false);
            this.serverInfo = new ServerInfo(
                    this.serverInfo.name,
                    this.serverInfo.description,
                    ((SocketChannel) this.key.channel()).socket().getInetAddress(),
                    this.serverInfo.map_id,
                    this.serverInfo.port,
                    this.serverInfo.player_count,
                    this.serverInfo.has_password
            );
        }
        return true;
    }

    @Override
    protected void handle_impl() {
        if (LobbyCallbacks.serverAnnounceCallback != null) {
            LobbyCallbacks.serverAnnounceCallback.onServerAnnounce(this.serverInfo, this.key);
        }
    }

}
