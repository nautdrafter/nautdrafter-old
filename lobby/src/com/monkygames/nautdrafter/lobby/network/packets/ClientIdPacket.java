/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.network.packets.AbstractClientIDPacket;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.lobby.network.UserDetails;
import com.monkygames.nautdrafter.lobby.network.callbacks.LobbyCallbacks;
import java.nio.channels.SelectionKey;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The servers version of the ClientID packet
 */
public class ClientIdPacket extends AbstractClientIDPacket {

    private SelectionKey key;

    /**
     * Constructor creating a packet with a byte[] of data
     *
     * @param data
     * @param key
     */
    public ClientIdPacket(byte[] data, SelectionKey key) {
        super(data);
        this.key = key;
    }

    /**
     * Constructor creating a packet with a String of data
     *
     * @param clientid
     * @throws PacketEncoderException
     */
    public ClientIdPacket(String clientid) throws PacketEncoderException {
        super(clientid);
    }

    /**
     * When you get this packet, decode the new UserDetails
     */
    @Override
    protected void handle_impl() {
        UUID id = UUID.fromString(this.clientid);
        UserDetails ud = UserDetails.find(id);
        if (ud == null) {
            // user details not found
            try {
                this.response = new ClientIdPacket(this.clientid);
            } catch (PacketEncoderException ex) {
                Logger.getLogger(ClientIdPacket.class.getName()).log(Level.SEVERE, null, ex);
                this.disconnect = true;
            }
            if (LobbyCallbacks.clientIdentityCallback != null) {
                LobbyCallbacks.clientIdentityCallback.onClientUnknown(id, this.key);
            }
        } else {
            if (LobbyCallbacks.clientIdentityCallback != null) {
                LobbyCallbacks.clientIdentityCallback.onClientIdentify(ud.clientId, ud.steamId, this.key);
            }
        }
    }
}
