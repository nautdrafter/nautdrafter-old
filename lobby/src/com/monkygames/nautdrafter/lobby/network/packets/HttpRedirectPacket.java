/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.lobby.network.packets;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.Constants;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Sends an HTTP redirect
 */
public class HttpRedirectPacket extends Packet {

    public HttpRedirectPacket(String redirectTo) {
        byte[] content = ("<html><head><title>301 Moved Permanently</title></head><body><center><a href=\""
                + redirectTo
                + "\"><h1>301 Moved Permanently</h1></a></center></body></html>").getBytes(Packet.charset);
        byte[] headers = ("HTTP/1.1 301 Moved Permanently\r\n"
                + "Server: nautdrafter_lobby/" + Constants.VERSION + "\r\n"
                + "Date: " + new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss Z").format(new Date()) + "\r\n"
                + "Content-Type: text/html\r\n"
                + "Content-Length: " + content.length + "\r\n"
                + "Connection: close\r\n"
                + "Location: " + redirectTo + "\r\n\r\n").getBytes(Packet.charset);
        this.data = new byte[headers.length + content.length];
        System.arraycopy(headers, 0, this.data, 0, headers.length);
        System.arraycopy(content, 0, this.data, headers.length, content.length);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    protected void handle_impl() {
        throw new UnsupportedOperationException("Not supported");
    }

}
