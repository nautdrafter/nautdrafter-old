/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.lobby.network.callbacks;

import com.monkygames.nautdrafter.common.network.ServerInfo;
import java.nio.channels.SelectionKey;

/**
 * Used for notifying the Lobby Server of a new Drafting Server. By nature of being a callback, allows accessing members
 * of the Lobby Server. <br>
 * This callback is not buffered.
 */
public interface ServerAnnounceCallback {

    public void onServerAnnounce(ServerInfo info, SelectionKey key);

    public void onServerUpdate(int playerCount, SelectionKey key);
}
