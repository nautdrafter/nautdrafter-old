/*
 * Copyright (C) 2014 Nautdrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.lobby.network;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.UserDetailsPacket;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 * Used to handle all of the interaction with Steams API, OpenID and Community
 */
public class SteamAPI {

    /**
     * Reads the API key from a file, id it is missing it will ask the user to get it
     *
     * @return
     */
    private static String getKey() {
        try {
            return new String(Files.readAllBytes(Paths.get("STEAM_API_KEY")), Packet.charset).trim();
        } catch (IOException ex) {
            System.err.println("You need to create a file named \"STEAM_API_KEY\" in the same folder as \"NautDrafter_Lobby.jar\", containing your Steam API Key");
            System.exit(-1);
            return null;
        }
    }
    private static final String API_KEY = getKey();
    private static final String PROFILE_API_URL = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=" + API_KEY + "&steamids=";

    /**
     * This will be called once Steam has finished authenticating and will finish off the process
     *
     * @param clientId
     * @param steamId
     * @return
     */
    public static UserDetailsPacket getDetails(UUID clientId, Long steamId) {
        // Insert into database
        UserDetails ud = new UserDetails(clientId, steamId);
        Logger.getLogger(LobbyServer.class.getName()).log(Level.INFO, "Obtaining Steam username and Profile picture for {0}", steamId);
        try {
            // Get username and icon using Steam API
            URL JSONUrl = new URL(PROFILE_API_URL + steamId);
            BufferedReader br = new BufferedReader(new InputStreamReader(JSONUrl.openStream()));
            StringBuilder sb = new StringBuilder();
            int count;
            while ((count = br.read()) != -1) {
                sb.append((char) count);
            }
            String jsonText = sb.toString();
            JSONObject parsed = new JSONObject(jsonText).getJSONObject("response").getJSONArray("players").getJSONObject(0);
            String steamName = parsed.getString("personaname");
            String avatarImg = parsed.getString("avatarmedium");
            // String[] split = avatarImg.split("http\\:\\/\\/media\\.steampowered\\.com\\/steamcommunity\\/public\\/images\\/avatars\\/");
            // String avatarId = split[1].substring(0, split[1].length() - 4);
            return new UserDetailsPacket(new Player(steamId, steamName, avatarImg));
        } catch (IOException ex) {
            Logger.getLogger(SteamAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void touch() {
    }
}
