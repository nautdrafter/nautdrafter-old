/*
 * Copyright (C) 2014 NautDrafter Lobby
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.lobby.network;

import java.io.File;
import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;
import org.mapdb.DB;
import org.mapdb.DBMaker;

/**
 * Retrieves user details from the database
 */
public class UserDetails {

    private static final DB db = DBMaker.newFileDB(new File("lobby.db")).asyncWriteEnable().closeOnJvmShutdown().make();
    private static final ConcurrentNavigableMap<UUID, Long> users = db.getTreeMap("users");

    public final UUID clientId;
    public final Long steamId;
    
    /**
     * Update or insert a UserDetails with a known clientId and steamId
     * @param clientId
     * @param steamId
     */
    public UserDetails(UUID clientId, long steamId) {
        this.clientId = clientId;
        this.steamId = steamId;
        users.put(clientId, steamId);
        db.commit();
    }

    /**
     * Internal constructor used by UserDetails.find
     *
     * @param clientId
     * @param steamId
     */
    private UserDetails(UUID clientId, Long steamId) {
        this.clientId = clientId;
        this.steamId = steamId;
    }

    /**
     * Finds the steamId for a given clientId, if any
     *
     * @param clientId
     * @return
     */
    public static UserDetails find(UUID clientId) {
        Long steamId = users.get(clientId);
        if (steamId == null) {
            return null;
        }
        return new UserDetails(clientId, steamId);
    }

    public static void touch() {
    }
}
