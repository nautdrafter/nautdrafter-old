/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network;

import com.monkygames.nautdrafter.common.network.Server;
import com.monkygames.nautdrafter.common.network.packets.HttpHeaderPacket;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;

/**
 * Webserver... because why not?
 */
public class WebServer extends Server {

    public static class WebServerPacket extends HttpHeaderPacket {

        private final int num;

        public WebServerPacket(int num, byte[] data) {
            super(data);
            this.num = num;
        }

        @Override
        public void handle_impl() {
            super.handle_impl();
            this.response = new HttpHeaderPacket(("HTTP/1.1 200 OK\r\n\r\nTHIS WAS REQUEST #" + this.num).getBytes(Packet.charset));
            this.disconnect = true;
        }
    }

    int request_count = 1;

    public WebServer(int port) throws IOException {
        super(new InetSocketAddress(port));
    }

    @Override
    protected Packet createPacket(SelectionKey key, byte[] data) throws IOException {
        return new WebServerPacket(this.request_count++, data);
    }

    public static void main(String[] args) throws IOException {
        new WebServer(8080).run();
    }
}
