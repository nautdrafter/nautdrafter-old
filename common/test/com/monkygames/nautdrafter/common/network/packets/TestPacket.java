/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets;

import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Basic packet class for testing segments
 */
public class TestPacket extends Packet {

    private final int length;

    public TestPacket(byte[] data) throws PacketDecoderException {
        super(data);
        this.length = 0;
    }

    public TestPacket(byte[] data, int length) throws PacketDecoderException {
        super(data);
        this.length = length;
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        if (this.length == 0) {
            return true;
        }
        System.out.println("TestPacket decoding: " + this.available() + "/" + this.length);
        this.pos += this.available();
        return (this.pos > this.length);
    }

    @Override
    protected void handle_impl() {
        if (this.length > 0) {
            try {
                this.response = new TestPacket(Arrays.copyOfRange(this.data, 1, 1 + this.length));
                this.disconnect = true;
            } catch (PacketDecoderException ex) {
                Logger.getLogger(TestPacket.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
