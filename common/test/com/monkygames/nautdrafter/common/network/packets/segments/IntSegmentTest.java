/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.packets.TestPacket;
import com.monkygames.nautdrafter.common.network.packets.segments.IntSegment;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @see com.monkygames.nautdrafter.network.packets.segments.IntSegment
 */
public class IntSegmentTest {

    public IntSegmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encoding and decoding, of class IntSegment
     */
    @Test
    public void testSegment() throws Exception {
        Random rng = new Random();
        // test ints of every valid size
        for (int i = 1; i <= 8; i++) {
            long val = rng.nextLong();
            if (i < 8) {
                val &= ((1l << (i * 8)) - 1);
            }
            IntSegment instance = new IntSegment(i);
            byte[] encoded = instance.encode(val);
            long decoded = (long) instance.decode(new TestPacket(encoded));
            assertEquals(val, decoded);
        }
    }

}
