/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets;

import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.Constants;
import java.util.Arrays;

/**
 * Uses some HTTP headers to get through Openshift's virtual host routing
 */
public class HttpHeaderPacket extends Packet {

    public static final byte[] LobbyConnect = ("GET / HTTP/1.1\r\nHost: " + Constants.LOBBY_ADDRESS + "\r\n\r\n").getBytes(Packet.charset);

    public HttpHeaderPacket(byte[] data) {
        super(data);
        this.decode_from = 0;
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        int max = this.max();
        for (; this.decode_from < max; this.decode_from++) {
            if (this.decode_from < 4) {
                continue;
            }
            // read until the end of the HTTP header (\n\n or \r\n\r\n)
            if (this.data[this.decode_from] == 10
                    && ((this.data[this.decode_from - 1] == 10)
                    || (this.data[this.decode_from - 1] == 13
                    && this.data[this.decode_from - 2] == 10
                    && this.data[this.decode_from - 3] == 13))) {
                this.pos = this.decode_from;
                return true;
            }
            // if this is not a HTTP header,
            // then the input buffer will fill up and cause an exception,
            // and we will disconnect
        }
        this.pos = this.decode_from;
        return false;
    }

    @Override
    protected void handle_impl() {
        System.out.println(new String(this.data, 0, this.pos, Packet.charset));
        this.response = new HttpHeaderPacket(Arrays.copyOf(this.data, this.pos));
    }

}
