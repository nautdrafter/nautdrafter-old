/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.TestPacket;
import com.monkygames.nautdrafter.common.network.packets.segments.PlayerSegment;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @see com.monkygames.nautdrafter.network.packets.segments.PlayerSegment
 */
public class PlayerSegmentTest {

    public PlayerSegmentTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encoding and decoding, of class RawSegment
     */
    @Test
    public void testSegment() throws Exception {
        Random rng = new Random();
        Player player = new Player(rng.nextLong(), "Player", "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg");
        PlayerSegment instance = new PlayerSegment();
        byte[] encoded = instance.encode(player);
        Player decoded = (Player) instance.decode(new TestPacket(encoded));
        assertEquals(player.steamId, decoded.steamId);
        assertEquals(player.name, decoded.name);
        assertEquals(player.iconUrl, decoded.iconUrl);
    }

}
