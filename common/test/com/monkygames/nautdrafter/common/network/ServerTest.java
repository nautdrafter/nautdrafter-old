/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.common.network;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Arrays;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertArrayEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * TODO: write documentation for ServerTest
 */
public class ServerTest {

    public ServerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSendRecv() throws Exception {
        TestServer server = new TestServer(null);

        new Thread() {

            @Override
            public void run() {
                server.run();
            }
        }.start();

        Socket client = new Socket(InetAddress.getLocalHost(), server.getPort());
        Random rng = new Random();
        byte[] data = new byte[100];
        rng.nextBytes(data);
        OutputStream out = client.getOutputStream();
        out.write(0); // header
        for (byte b : data) {
            out.write(b);
            Thread.sleep(10);
        }
        System.out.println("sent");
        InputStream in = client.getInputStream();
        int i = 0, b;
        byte[] recv = new byte[10240];
        while ((b = in.read()) != -1) {
            recv[i++] = (byte) b;
        }
        System.out.println("received");
        recv = Arrays.copyOf(recv, i);
        assertArrayEquals(data, recv);
    }

}