/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network;

import java.net.InetAddress;

/**
 * Describes a server that we could connect to Note: This means a "game" (drafting) room, NOT a lobby
 */
public class ServerInfo {

    public final String name, description;
    public final InetAddress address;
    public final int port;
    public final int map_id;
    public final boolean has_password;
    public int player_count;

    /**
     * @param name
     * @param description
     * @param address
     * @param map_id
     * @param port
     * @param player_count
     * @param has_password
     */
    public ServerInfo(String name, String description, InetAddress address, int map_id, int port, int player_count, boolean has_password) {
        this.name = name;
        this.description = description;
        this.address = address;
        this.map_id = map_id;
        this.port = port;
        this.player_count = player_count;
        this.has_password = has_password;
    }
}
