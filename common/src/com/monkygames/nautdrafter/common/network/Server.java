/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Base server class
 */
public abstract class Server {

    /**
     * Stores information about a connected client.
     */
    public static class ConnectionInfo {

        private static final int BUFFER_SIZE = 10240;

        public final ByteBuffer recv_buffer;
        public final ByteBuffer send_buffer;
        public Packet decoding;
        public boolean kill = false;

        public ConnectionInfo() {
            this.recv_buffer = ByteBuffer.allocate(BUFFER_SIZE);
            this.send_buffer = ByteBuffer.allocate(BUFFER_SIZE);
            this.decoding = null;
        }

        protected ConnectionInfo(ConnectionInfo shallow_copy) {
            this.recv_buffer = shallow_copy.recv_buffer;
            this.send_buffer = shallow_copy.send_buffer;
            this.decoding = shallow_copy.decoding;
            this.kill = shallow_copy.kill;
        }
    }

    protected final Selector selector;
    protected final ServerSocketChannel ssc;

    protected Server(InetSocketAddress addr) throws IOException {
        this.ssc = ServerSocketChannel.open();
        this.selector = Selector.open();
        this.ssc.socket().setReuseAddress(true);
        ssc.socket().bind(addr);
        Logger.getLogger(Server.class.getName()).log(Level.INFO, "Server starting on {0}:{1}", new Object[]{ssc.socket().getInetAddress().getHostAddress(), ssc.socket().getLocalPort()});
        ssc.configureBlocking(false);
        ssc.register(selector, SelectionKey.OP_ACCEPT);
    }

    public int getPort() {
        return this.ssc.socket().getLocalPort();
    }

    public InetAddress getAddress() {
        return this.ssc.socket().getInetAddress();
    }

    /**
     * Closes a connection and performs any relevant cleanup
     *
     * @param key
     */
    protected void kill(SelectionKey key) {
        try {
            ((SocketChannel) key.channel()).socket().close();
            key.channel().close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Accepts a connection (either a server or a client looking for a server)
     *
     * @param key
     * @throws IOException
     */
    protected void accept(SelectionKey key) throws IOException {
        SocketChannel sc = ((ServerSocketChannel) key.channel()).accept();
        sc.configureBlocking(false);
        sc.register(this.selector, SelectionKey.OP_READ, new ConnectionInfo());
    }

    /**
     * Creates a new packet based on the packet id
     *
     * @param key reference to the client
     * @param data the packet data
     * @return the new packet, or null if there was an error
     * @throws java.io.IOException
     */
    protected abstract Packet createPacket(SelectionKey key, byte[] data) throws IOException;

    /**
     * Handles data received from a host
     *
     * @param key
     * @throws Exception
     */
    protected void read(SelectionKey key) throws Exception {
        SocketChannel channel = (SocketChannel) key.channel();
        ConnectionInfo info = (ConnectionInfo) key.attachment();
        boolean extra_iter = false; // used to force an extra iteration when a packet has been read and data remains
        int read_val;
        while (extra_iter || (read_val = channel.read(info.recv_buffer)) > 0) {
            extra_iter = false;

            // create the packet if we're at the start of a new packet
            if (info.decoding == null) {
                info.decoding = this.createPacket(key, info.recv_buffer.array());
                if (info.decoding == null) {
                    this.kill(key);
                    return;
                }
            }
            // try to decode (or continue decoding) the packet

            // buffer position is the number of bytes in the buffer...
            // aka the number of bytes available to decode
            info.decoding.setAvailable(info.recv_buffer.position());
            // if the packet is fully decoded, handle it
            if (info.decoding.decode()) {
                info.decoding.handle();
                // in case the attachment changed
                info = (ConnectionInfo) key.attachment();
                // don't kill immediately
                info.kill = info.decoding.getDisconnect();
                // if there is a response packet to be sent, send it
                if (info.decoding.hasResponse()) {
                    // if the buffer overflows, the connection will be killed
                    info.send_buffer.put(info.decoding.getResponse().data);
                    this.write(key);
                } else if (info.kill) {
                    // can kill immediately since there is no response to send
                    this.kill(key);
                    return;
                }
                // retain any remaining data in the buffer
                if (info.decoding.pos < info.recv_buffer.position()) {
                    byte[] remainder = new byte[info.recv_buffer.position() - info.decoding.pos];
                    info.recv_buffer.position(info.decoding.pos);
                    info.recv_buffer.get(remainder);
                    info.recv_buffer.clear();
                    info.recv_buffer.put(remainder);
                    extra_iter = true;
                } else {
                    info.recv_buffer.clear();
                }
                // we're not decoding that packet anymore
                info.decoding = null;
            }
        }
        if (read_val == -1) {
            kill(key);
        }
    }

    protected void write(SelectionKey key) {
        try {
            ConnectionInfo info = (ConnectionInfo) key.attachment();
            SocketChannel channel = (SocketChannel) key.channel();
            if (!channel.isConnected()) {
                this.kill(key);
                return;
            }
            if (info.send_buffer.position() > 0) {
                // go to start of buffer
                info.send_buffer.flip();
                // write any data
                channel.write(info.send_buffer);
                // move any unwritten data to the start of the buffer
                info.send_buffer.compact();
            }
            // check if we have more data to write
            // if so, we need to listen to the OP_WRITE (ready to write) event
            if (info.send_buffer.position() > 0) {
                key.interestOps(key.interestOps() | SelectionKey.OP_WRITE);
            } else {
                key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE);
                // once we've written everything, we can kill the client
                if (info.kill) {
                    this.kill(key);
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            this.kill(key);
        }
    }

    public void run() {
        Iterator<SelectionKey> iter;
        SelectionKey key;
        while (ssc.isOpen()) {
            try {
                selector.select();
                iter = selector.selectedKeys().iterator();
                while (iter.hasNext()) {
                    key = iter.next();
                    iter.remove();
                    try {
                        if (key.isAcceptable()) {
                            this.accept(key);
                        }
                        if (key.isWritable()) {
                            this.write(key);
                        }
                        if (key.isReadable()) {
                            this.read(key);
                        }
                    } catch (Exception ex) {
                        this.kill(key);
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void shutdown() {
        try {
            this.selector.wakeup();
            this.ssc.socket().close();
            this.ssc.close();
            this.selector.close();
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
