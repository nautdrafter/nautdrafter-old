/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network;

/**
 * Describes a player
 */
public class Player {

    public final Long steamId;
    public final String name;
    public final String iconUrl;

    public Player(long steamId, String name, String iconUrl) {
        this.steamId = steamId;
        this.name = name;
        this.iconUrl = iconUrl;
    }

    public Player(String name, String iconUrl) {
        this.name = name;
        this.iconUrl = iconUrl;
        this.steamId = null;
    }
}
