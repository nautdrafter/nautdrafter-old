/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.common.network.packets;

import java.nio.charset.Charset;

/**
 * Generic packet class
 */
public abstract class Packet {

    public static final byte CHAT_PACKET_ID = 0;
    public static final byte PLAYER_JOINED_PACKET_ID = 1;
    public static final byte PLAYER_LEFT_PACKET_ID = 2;
    public static final byte ROOM_INFO_PACKET_ID = 3;

    // packet constants used for communication with the lobby server
    public static final byte SERVER_ANNOUNCE_PACKET_ID = 4;
    public static final byte SERVER_UPDATE_PACKET_ID = 5;
    public static final byte SERVER_LIST_REQUEST_PACKET_ID = 7;
    public static final byte SERVER_LIST_PACKET_ID = 8;
    public static final byte CLIENTID_PACKET_ID = 9;
    public static final byte NEWAUTH_PACKET_ID = 10;
    public static final byte USERDETAILS_PACKET_ID = 11;
    public static final byte HTTP_GET_REQUEST_ID = 71; // ASCII 'G'

    public static final Charset charset = Charset.forName("UTF-8");

    /**
     * The raw bytes of the packet
     */
    public byte[] data;

    /**
     * The current position in the packet
     */
    public int pos;
    /**
     * The number of bytes available (used when the packet represents a bytebuffer)
     */
    private int available;
    /**
     * Whether the packet has already been decoded
     */
    protected boolean decoded = true;
    /**
     * Where to start decoding from
     */
    protected int decode_from = 1;
    /**
     * Response, if any
     */
    protected Packet response = null;
    /**
     * Whether to disconnect
     */
    protected boolean disconnect = false;

    /**
     * Create a packet from objects and encode it to byte data
     */
    protected Packet() {
    }

    /**
     * Create a packet from the network data and decode it to its component Objects
     *
     * @param data
     * @param available
     */
    protected Packet(byte[] data, int available) {
        this.data = data;
        this.available = available;
        this.decoded = false;
    }

    /**
     * Create a packet from the network data and decode it to its component Objects
     *
     * @param data
     */
    protected Packet(byte[] data) {
        this(data, Integer.MAX_VALUE);
    }

    public int max() {
        return Math.min(this.data.length, this.available);
    }

    public int available() {
        return this.max() - this.pos;
    }

    public void setAvailable(int available) {
        this.available = available;
    }

    protected abstract boolean decode_impl() throws PacketDecoderException;

    /**
     * Decodes the packet from byte[] to Objects
     *
     * @return true if the packet was fully decoded
     * @throws com.monkygames.nautdrafter.network.packets.PacketDecoderException
     */
    public final boolean decode() throws PacketDecoderException {
        if (this.decoded) {
            return true;
        }
        this.pos = this.decode_from;
        this.decoded = this.decode_impl();
        return this.decoded;
    }

    protected abstract void handle_impl();

    /**
     * Handles the packet
     */
    public void handle() {
        // can't handle if not decoded
        if (!this.decoded) {
            return;
        }
        this.handle_impl();
    }

    public boolean hasResponse() {
        return (this.response != null);
    }

    public Packet getResponse() {
        return this.response;
    }

    public boolean getDisconnect() {
        return this.disconnect;
    }
}
