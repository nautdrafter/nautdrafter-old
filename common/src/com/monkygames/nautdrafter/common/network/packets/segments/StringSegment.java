/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;

/**
 * Represents a string as an array of bytes, optionally with a header containing the length
 */
public class StringSegment {

    /**
     * Number of bytes used to store the data (or to store the length if it is variable)
     */
    public final int lengthBytes;
    /**
     * Whether the length is variable
     */
    public final boolean variableLength;

    /**
     * Create a StringSegment, optionally of variable length
     *
     * @param lengthBytes the length in bytes if not variable, or the number of bytes used to store the length
     * @param variableLength whether the length is variable
     */
    public StringSegment(int lengthBytes, boolean variableLength) {
        this.lengthBytes = lengthBytes;
        this.variableLength = variableLength;
    }

    /**
     * Create a StringSegment of fixed length in bytes
     *
     * @param lengthBytes
     */
    public StringSegment(int lengthBytes) {
        this(lengthBytes, false);
    }

    /**
     * Encodes a string
     *
     * @param str string to encode
     * @param length length in bytes, or number of bytes for storing the length if variable
     * @param variableLength whether the length is variable
     * @return Encoded data
     * @throws PacketEncoderException
     */
    public static byte[] encode(String str, int length, boolean variableLength) throws PacketEncoderException {
        return RawSegment.encode(str.getBytes(Packet.charset), length, variableLength);
    }

    /**
     * Decodes a String from the packet
     *
     * @param packet The raw packet
     * @param length
     * @param variableLength
     * @return The decoded string
     * @throws PacketDecoderException
     */
    public static String decode(Packet packet, int length, boolean variableLength) throws PacketDecoderException {
        int offset = packet.pos;
        byte[] data = packet.data;
        if (variableLength) {
            int len = RawSegment.decodeVariableLength(data, offset, length);
            offset += length;
            length = len;
        }
        if (data.length - offset < length) {
            throw new PacketDecoderException("Invalid packet length");
        }
        packet.pos = offset + length;
        return new String(data, offset, length, Packet.charset);
    }

    static boolean can_decode(Packet packet, int length, boolean variableLength, boolean advance_pos) {
        return RawSegment.can_decode(packet, length, variableLength, advance_pos);
    }

    public static boolean can_decode(Packet packet, int length, boolean variableLength) {
        return can_decode(packet, length, variableLength, false);
    }

    public byte[] encode(String val) throws PacketEncoderException {
        return encode(val, this.lengthBytes, this.variableLength);
    }

    public String decode(Packet packet) throws PacketDecoderException {
        return decode(packet, this.lengthBytes, this.variableLength);
    }

    public boolean can_decode(Packet packet) {
        return can_decode(packet, this.lengthBytes, this.variableLength);
    }
}
