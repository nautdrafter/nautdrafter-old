/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.common.network.packets;

import com.monkygames.nautdrafter.common.network.ServerInfo;
import com.monkygames.nautdrafter.common.network.packets.segments.ServerInfoSegment;

/**
 * Used for Drafting Server to announce themselves to Lobby Server
 */
public class ServerAnnouncePacketEncoder extends Packet {

    private static final int PACKET_ID = SERVER_ANNOUNCE_PACKET_ID;

    protected ServerInfo serverInfo;

    protected ServerAnnouncePacketEncoder(byte[] data) throws PacketDecoderException {
        super(data);
    }

    public ServerAnnouncePacketEncoder(ServerInfo serverInfo) throws PacketEncoderException {
        this.serverInfo = serverInfo;

        byte[] infoBuffer = ServerInfoSegment.encode(this.serverInfo, false);
        this.data = new byte[1 + infoBuffer.length];
        this.data[0] = PACKET_ID;
        System.arraycopy(infoBuffer, 0, this.data, 1, infoBuffer.length);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    protected void handle_impl() {
        throw new UnsupportedOperationException("Not supported");
    }
}
