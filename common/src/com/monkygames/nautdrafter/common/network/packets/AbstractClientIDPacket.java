/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets;

import com.monkygames.nautdrafter.common.network.packets.segments.StringSegment;

/**
 * Used by both the client and server to contain the ClientID inside a packet
 */
public abstract class AbstractClientIDPacket extends Packet {

    protected String clientid;
    private static final int PACKET_ID = Packet.CLIENTID_PACKET_ID;
    private static final StringSegment clientidSegment = new StringSegment(1, true);

    /**
     * The constructor taking the ClientID in byte[] form
     * @param data 
     */
    public AbstractClientIDPacket(byte[] data) {
        super(data);
    }

    /**
     * The constructor taking the ClientID in String form
     * @param clientid
     * @throws PacketEncoderException 
     */
    public AbstractClientIDPacket(String clientid) throws PacketEncoderException {
        this.clientid = clientid;

        byte[] clientidBuffer = clientidSegment.encode(this.clientid);
        this.data = new byte[1 + clientidBuffer.length];
        this.data[0] = PACKET_ID;
        System.arraycopy(clientidBuffer, 0, this.data, 1, clientidBuffer.length);
    }

    /**
     * Used to decode a ClientID packet
     * @return
     * @throws PacketDecoderException 
     */
    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        if (this.clientid == null) {
            if (!clientidSegment.can_decode(this)) {
                return false;
            }
            this.clientid = clientidSegment.decode(this);
        }
        return true;
    }
    
    /**
     * Returns the private clientID
     * @return 
     */
    public String getClientId() {
        return this.clientid;
    }
}
