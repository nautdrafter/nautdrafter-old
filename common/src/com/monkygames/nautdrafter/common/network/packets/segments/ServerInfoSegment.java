/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.ServerInfo;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * TODO: write documentation for ServerInfoSegment
 */
public class ServerInfoSegment {

    /**
     * Encodes a ServerInfo
     *
     * @param server
     * @param include_addr
     * @return Encoded data
     * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
     */
    public static byte[] encode(ServerInfo server, boolean include_addr) throws PacketEncoderException {
        byte[] nameBuffer = StringSegment.encode(server.name, 1, true);
        byte[] descriptionBuffer = StringSegment.encode(server.description, 1, true);
        byte[] ipBuffer;
        if (include_addr) {
            ipBuffer = RawSegment.encode(server.address.getAddress(), 1, true);
        } else {
            ipBuffer = new byte[0];
        }

        // name, description, ip, map_id, port, player_count
        byte[] data = new byte[nameBuffer.length + descriptionBuffer.length + ipBuffer.length + 1 + 2 + 1 + 1];
        System.arraycopy(nameBuffer, 0, data, 0, nameBuffer.length);
        System.arraycopy(descriptionBuffer, 0, data, nameBuffer.length, descriptionBuffer.length);
        System.arraycopy(ipBuffer, 0, data, nameBuffer.length + descriptionBuffer.length, ipBuffer.length);
        int pos = nameBuffer.length + descriptionBuffer.length + ipBuffer.length;
        data[pos++] = (byte) ((server.map_id) & 0xFF);
        data[pos++] = (byte) ((server.port >> 8) & 0xFF);
        data[pos++] = (byte) ((server.port) & 0xFF);
        data[pos++] = (byte) ((server.player_count) & 0xFF);
        data[pos++] = (byte) ((server.has_password) ? 1 : 0);

        return data;
    }

    /**
     * Encodes a ServerInfo
     *
     * @param server
     * @return Encoded data
     * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
     */
    public static byte[] encode(ServerInfo server) throws PacketEncoderException {
        return encode(server, true);
    }

    /**
     * Decodes a player from the packet
     *
     * @param packet
     * @param include_addr
     * @return
     * @throws PacketDecoderException
     */
    public static ServerInfo decode(Packet packet, boolean include_addr) throws PacketDecoderException {
        String name = StringSegment.decode(packet, 1, true);
        String description = StringSegment.decode(packet, 1, true);
        InetAddress addr = null;
        if (include_addr) {
            try {
                addr = InetAddress.getByAddress(RawSegment.decode(packet, 1, true));
            } catch (UnknownHostException ex) {
                throw new PacketDecoderException("Failed to decode address");
            }
        }
        int map_id = (int) IntSegment.decode(packet, 1);
        int port = (int) IntSegment.decode(packet, 2);
        int player_count = (int) IntSegment.decode(packet, 1);
        int has_password = (int) IntSegment.decode(packet, 1);
        return new ServerInfo(name, description, addr, map_id, port, player_count, has_password != 0);
    }

    /**
     * Decodes a player from the packet
     *
     * @param packet
     * @return
     * @throws PacketDecoderException
     */
    public static ServerInfo decode(Packet packet) throws PacketDecoderException {
        return decode(packet, true);
    }

    static boolean can_decode(Packet packet, boolean include_addr, boolean advance_pos) {
        int pos = packet.pos;
        boolean can = (StringSegment.can_decode(packet, 1, true, true)
                && StringSegment.can_decode(packet, 1, true, true)
                && (!include_addr || RawSegment.can_decode(packet, 1, true, true))
                && packet.available() >= 5);
        if (!advance_pos) {
            // if we're not advancing the position, restore to original position
            packet.pos = pos;
        }
        return can;
    }

    public static boolean can_decode(Packet packet) {
        return can_decode(packet, false, false);
    }

    public static boolean can_decode(Packet packet, boolean include_addr) {
        return can_decode(packet, include_addr, false);
    }
}
