/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import java.util.Arrays;

/**
 * Prepends a header containing the length, to an array of bytes
 */
public class RawSegment {

    /**
     * Number of bytes used to store the data (or to store the length if it is variable)
     */
    public final int lengthBytes;
    /**
     * Whether the length is variable
     */
    public final boolean variableLength;

    /**
     * Create a RawSegment, optionally of variable length
     *
     * @param lengthBytes the length in bytes if not variable, or the number of bytes used to store the length
     * @param variableLength whether the length is variable
     */
    public RawSegment(int lengthBytes, boolean variableLength) {
        this.lengthBytes = lengthBytes;
        this.variableLength = variableLength;
    }

    /**
     * Create a RawSegment of fixed length in bytes
     *
     * @param lengthBytes
     */
    public RawSegment(int lengthBytes) {
        this(lengthBytes, false);
    }

    /**
     * Encodes a raw sequence of bytes
     *
     * @param raw data to encode
     * @param length length in bytes, or number of bytes for storing the length if variable
     * @param variableLength whether the length is variable
     * @return Encoded data
     * @throws com.monkygames.nautdrafter.network.packets.PacketEncoderException
     */
    public static byte[] encode(byte[] raw, int length, boolean variableLength) throws PacketEncoderException {
        if (variableLength) {
            if (raw.length >= (2L << (length * 8))) {
                throw new PacketEncoderException("Value was too long");
            }
            byte[] data = new byte[length + raw.length];
            for (int i = 0; i < length; i++) {
                data[i] = (byte) ((raw.length >> ((length - (i + 1)) * 8)) & 0xFF);
            }
            System.arraycopy(raw, 0, data, length, raw.length);
            return data;
        } else {
            return raw;
        }
    }

    public static int decodeVariableLength(byte[] data, int offset, int bytes) {
        int len = 0;
        for (int i = 0; i < bytes; i++) {
            len |= (data[i + offset] & 0xFF) << ((bytes - (i + 1)) * 8);
        }
        return len;
    }

    /**
     * Gets some raw data from the packet
     *
     * @param packet The raw packet
     * @param length
     * @param variableLength
     * @return The extracted data
     * @throws PacketDecoderException
     */
    public static byte[] decode(Packet packet, int length, boolean variableLength) throws PacketDecoderException {
        int offset = packet.pos;
        byte[] data = packet.data;
        if (variableLength) {
            int len = decodeVariableLength(data, offset, length);
            offset += length;
            length = len;
        }
        if (data.length - offset < length) {
            throw new PacketDecoderException("Invalid packet length");
        }
        packet.pos = offset + length;
        return Arrays.copyOfRange(data, offset, length + offset);
    }

    static boolean can_decode(Packet packet, int length, boolean variableLength, boolean advance_pos) {
        // don't try to decode any further if not enough data
        int available = packet.available();
        if (available < length) {
            return false;
        }
        if (variableLength) {
            length += decodeVariableLength(packet.data, packet.pos, length);
        }
        if (available < length) {
            return false;
        }
        if (advance_pos) {
            packet.pos += length;
        }
        return true;
    }

    public static boolean can_decode(Packet packet, int length, boolean variableLength) {
        return can_decode(packet, length, variableLength, false);
    }

    public byte[] encode(byte[] val) throws PacketEncoderException {
        return encode(val, this.lengthBytes, this.variableLength);
    }

    public byte[] decode(Packet packet) throws PacketDecoderException {
        return decode(packet, this.lengthBytes, this.variableLength);
    }

    public boolean can_decode(Packet packet) {
        return can_decode(packet, this.lengthBytes, this.variableLength);
    }
}
