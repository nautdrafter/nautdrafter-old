/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;

/**
 * Represents a player as an array of bytes (composed of IntSegment and StringSegment)
 */
public class PlayerSegment {

    /**
     * Encodes a player
     *
     * @param player
     * @return Encoded data
     * @throws com.monkygames.nautdrafter.common.network.packets.PacketEncoderException
     */
    public static byte[] encode(Player player) throws PacketEncoderException {
        byte[] idBuffer = IntSegment.encode(player.steamId != null ? player.steamId : 0, 8);
        byte[] nameBuffer = StringSegment.encode(player.name, 1, true);
        byte[] iconBuffer = StringSegment.encode(player.iconUrl, 2, true);
        byte[] data = new byte[idBuffer.length + nameBuffer.length + iconBuffer.length];
        System.arraycopy(idBuffer, 0, data, 0, idBuffer.length);
        System.arraycopy(nameBuffer, 0, data, idBuffer.length, nameBuffer.length);
        System.arraycopy(iconBuffer, 0, data, idBuffer.length + nameBuffer.length, iconBuffer.length);
        return data;
    }

    /**
     * Decodes a player from the packet
     *
     * @param packet
     * @return
     * @throws PacketDecoderException
     */
    public static Player decode(Packet packet) throws PacketDecoderException {
        long steamId = IntSegment.decode(packet, 8);
        String name = StringSegment.decode(packet, 1, true);
        String iconUrl = StringSegment.decode(packet, 2, true);
        return new Player(steamId, name, iconUrl);
    }

    static boolean can_decode(Packet packet, boolean advance_pos) {
        int pos = packet.pos;
        boolean can = (IntSegment.can_decode(packet, 8, true)
                && StringSegment.can_decode(packet, 1, true, true)
                && StringSegment.can_decode(packet, 2, true, true));
        if (!advance_pos) {
            // if we're not advancing the position, restore to original position
            packet.pos = pos;
        }
        return can;
    }

    public static boolean can_decode(Packet packet) {
        return can_decode(packet, false);
    }
}
