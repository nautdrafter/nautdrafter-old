/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.segments.PlayerSegment;

/**
 * Encodes a packet that will contain all of the details from Steam
 */
public class UserDetailsPacket extends Packet {

    protected Player player;

    public Player getPlayer() {
        return this.player;
    }

    protected UserDetailsPacket(byte[] data) throws PacketDecoderException {
        super(data);
    }

    protected UserDetailsPacket(Player player, byte packet_id) throws PacketEncoderException {
        this.player = player;

        byte[] playerBuffer = PlayerSegment.encode(this.player);
        this.data = new byte[1 + playerBuffer.length];
        this.data[0] = packet_id;
        System.arraycopy(playerBuffer, 0, data, 1, playerBuffer.length);
    }

    public UserDetailsPacket(Player player) throws PacketEncoderException {

        this(player, Packet.USERDETAILS_PACKET_ID);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        // player
        if (this.player == null) {
            if (!PlayerSegment.can_decode(this)) {
                return false;
            }
            this.player = PlayerSegment.decode(this);
            this.decode_from = this.pos;
        }
        return true;
    }

    @Override
    protected void handle_impl() {
        throw new UnsupportedOperationException("Not supported");
    }
}
