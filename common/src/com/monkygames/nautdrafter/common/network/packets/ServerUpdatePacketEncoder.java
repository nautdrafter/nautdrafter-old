/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.common.network.packets;

/**
 * Updates the player count for a Drafting Server
 */
public class ServerUpdatePacketEncoder extends Packet {

    private static final int PACKET_ID = SERVER_UPDATE_PACKET_ID;

    protected ServerUpdatePacketEncoder(byte[] data) throws PacketDecoderException {
        super(data);
    }

    public ServerUpdatePacketEncoder(int playerCount) throws PacketEncoderException {

        this.data = new byte[1 + 1];
        this.data[0] = PACKET_ID;
        this.data[1] = (byte) (playerCount & 0xFF);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        throw new UnsupportedOperationException("Not supported");
    }

    @Override
    protected void handle_impl() {
        throw new UnsupportedOperationException("Not supported");
    }
}
