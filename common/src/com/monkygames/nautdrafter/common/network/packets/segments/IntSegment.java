/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common.network.packets.segments;

import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;

/**
 * Represents an integer as an array of bytes
 */
public class IntSegment {

    /**
     * Number of bytes used to store the data (or to store the length if it is variable)
     */
    public final int lengthBytes;

    /**
     * Create an IntSegment of fixed length in bytes
     *
     * @param lengthBytes
     */
    public IntSegment(int lengthBytes) {
        this.lengthBytes = lengthBytes;
    }

    /**
     * Encodes an integer.
     *
     * @param val integer to encode
     * @param length Fixed length in number of bytes from the integer to store (max = 8 bytes)
     * @return Encoded data
     * @throws PacketEncoderException
     */
    public static byte[] encode(long val, int length) throws PacketEncoderException {
        byte[] data = new byte[length];
        for (int i = 0; i < length; i++) {
            data[i] = (byte) ((val >> ((length - (i + 1)) * 8)) & 0xFF);
        }
        return data;
    }

    /**
     * Decodes an integer from the packet
     *
     * @param packet The raw packet
     * @param length Fixed length in number of bytes from the integer to store (max = 8 bytes)
     * @return The decoded integer
     * @throws PacketDecoderException
     */
    public static long decode(Packet packet, int length) throws PacketDecoderException {
        int offset = packet.pos;
        byte[] data = packet.data;
        if (data.length - offset < length) {
            throw new PacketDecoderException("Invalid packet length");
        }
        long val = 0;
        for (int i = 0; i < length; i++) {
            val |= (long) (data[i + offset] & 0xFF) << ((length - (i + 1)) * 8);
        }
        packet.pos += length;
        return val;
    }

    static boolean can_decode(Packet packet, int length, boolean advance_pos) {
        if (packet.available() < length) {
            return false;
        }
        if (advance_pos) {
            packet.pos += length;
        }
        return true;
    }

    public static boolean can_decode(Packet packet, int length) {
        return can_decode(packet, length, false);
    }

    public byte[] encode(long val) throws PacketEncoderException {
        return encode(val, this.lengthBytes);
    }

    public long decode(Packet packet) throws PacketDecoderException {
        return decode(packet, this.lengthBytes);
    }

    public boolean can_decode(Packet packet) {
        return can_decode(packet, this.lengthBytes);
    }
}
