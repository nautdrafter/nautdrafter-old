/*
 * Copyright (C) 2014 NautDrafter Common
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.common;

/**
 * General constants used by NautDrafter
 */
public class Constants {

    public static final String LOBBY_ADDRESS = "server.nautdrafter.monky-games.com";
    public static final int LOBBY_PORT = 50000;
    public static final String VERSION = "0.0.1";
    public static final String HOMEPAGE = "http://nautdrafter.bitbucket.org/";
    public static final String SUCCESS_PAGE = HOMEPAGE + "success.html";
    public static final String FAILURE_PAGE = HOMEPAGE + "failure.html";

    private Constants() {
    }
}
