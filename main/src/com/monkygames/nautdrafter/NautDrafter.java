/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter;

import com.monkygames.nautdrafter.controller.Base;
import com.monkygames.nautdrafter.controller.LoginScreen;
import com.monkygames.nautdrafter.controller.TeamSelectionScreen;
import com.monkygames.nautdrafter.drafting.network.DraftingServer;
import com.monkygames.nautdrafter.view.PopUp;
import com.monkygames.nautdrafter.view.SubScreen;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;

/**
 *
 * @author agkf1
 */
public class NautDrafter extends Application {

    private static NautDrafter _instance;

    private Preferences prefs;

    static Base root;
    static Stage stage;
    VBox adsVbox;

    private static Scene mainScene;
    private static ResourceBundle resource;

    @Override
    public void start(Stage stage) throws Exception {

        _instance = this;
        this.prefs = Preferences.userNodeForPackage(NautDrafter.class);
        
        
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);

        root = new Base();
        this.adsVbox = new VBox(new Text("Developed By"), new ImageView("/assets/images/monky.png"), new ImageView("/assets/images/arcton.png"));
        root.setAdArea(this.adsVbox);
        root.setTitleAreaVisable(false);

        root.setMain(new LoginScreen());
        stage.setTitle("Nautdrafter");

        NautDrafter.stage = stage;
        mainScene = new Scene(root);

        stage.setScene(mainScene);
        stage.sizeToScene();
        stage.show();
        stage.setResizable(false);

    }

    /**
     * Return the Base for the application
     *
     * @return
     */
    public static Base getBase() {
        return root;
    }

    /**
     * Set the size and resizable propety in on go
     *
     * @param width
     * @param height
     * @param isResizable
     */
    public void resizeScreen(int width, int height, boolean isResizable) {
        stage.setWidth(width);
        stage.setHeight(height);
        stage.setResizable(isResizable);
    }

    /**
     * Switch to another screen
     *
     * @param screen SubScreen to show
     * @param stack Set true if you want to add onto the screen stack rather than just replace the current screen
     */
    public static void setScreen(SubScreen screen, boolean stack) {

        if (stack) {
            root.addMain(screen);
        } else {
            root.setMain(screen);
        }

        // Make sure screen is centered
        stage.centerOnScreen();
    }

    /**
     * Start steam in default browser
     */
    public void startSteam() {
        this.getHostServices().showDocument("http://steampowered.com");
    }

    /**
     * Get the preferences of the Nautdrafter application
     *
     * @return Preferences object for the application
     */
    public static Preferences getPreferences() {
        return _instance.prefs;
    }

    /**
     * Get the instance of the Nautdrafter application
     *
     * @return The running instance of the application
     */
    public static NautDrafter getInstance() {
        return _instance;
    }

    /**
     * Get the resource file for strings
     * @return Bundle for the strings
     */
    public static ResourceBundle getBundle(){
        if(resource==null){
            resource = ResourceBundle.getBundle("bundles.Strings");
        }
        return resource;
    }
    
    /*
     *   Things to help with killing servers when closing aplication    *
     */
    private final ArrayList<DraftingServer> runningServers = new ArrayList<>();

    public void addNewDraftingServer(DraftingServer server) {
        runningServers.add(server);
    }

    /**
     * Shutdown any running drafting servers or notify user that there is one running
     */
    @Override
    public void stop() {
        if (runningServers.isEmpty()) {
            return;
        }

        System.out.println("Preparing for shutdown");

        new PopUp("Do you want to shutdown running server(s)!", "Closing servers", "Shut down server(s)", "Keep servers running", () -> {
            runningServers.stream().forEach((d) -> {
                d.shutdown();
            });
        }, () -> {
            new PopUp("When ready click \"Shutdown\" to kill servers", "Waiting", "Shutdown", () -> {
                runningServers.stream().forEach((d) -> {
                    d.shutdown();
                });
            }).showPopup();
        }).showPopup();
    }
}
