/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.view.ChatBox;
import com.monkygames.nautdrafter.view.SubScreen;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Adam
 */
public class TeamSelectionScreen extends VBox implements SubScreen {

    @FXML
    private Button leaveButton;
    @FXML
    private Button proceedButton;

    @FXML
    private TeamPicker redTeam;
    @FXML
    private TeamPicker blueTeam;

    @FXML
    private ChatBox chatBox;
    
    public TeamSelectionScreen() {
        // Load xml        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/monkygames/nautdrafter/view/TeamSelectionScreen.fxml"), NautDrafter.getBundle());
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        leaveButton.setOnAction(action -> NautDrafter.getBase().popMain());

        blueTeam.setToBeUnique(redTeam);

        blueTeam.setValidSelectionListner((position, type) -> {
            System.out.println("player " + (type ? "selected" : "de-selected") + " position " + position + " on blue team ");
        });

        redTeam.setValidSelectionListner((position, type) -> {
            System.out.println("player " + (type ? "selected" : "de-selected") + " position " + position + " on red team");
        });       
    }

    @Override
    public Node getRoot() {
        return this;
    }

    @Override
    public String getTitle() {
        return NautDrafter.getBundle().getString("team_selection.title");
    }

    @Override
    public void onShow() {
    }

    public interface ValidSelectionListner {

        static final int CAPTAIN = 0, CREW_1 = 1, CREW_2 = 2;
        static final boolean SELECT = true, DESELECT = false;

        public void onValidSelection(int position, boolean type);
    }
    
     public interface NameChangeListner {
        public void onNameChange(String newName);
    }
      
     
}
