/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.network.LobbyClient;
import com.monkygames.nautdrafter.network.callbacks.LoginDetailsCallback;
import com.monkygames.nautdrafter.view.LockerPane;
import com.monkygames.nautdrafter.view.PopUp;
import com.monkygames.nautdrafter.view.SubScreen;
import java.io.IOException;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Hyperlink;
import javafx.scene.layout.AnchorPane;

/**
 *
 * @author agkf1
 */
public class LoginScreen implements LoginDetailsCallback, SubScreen {

    @FXML
    private Button playerSignIn;
    @FXML
    private Button spectatorSignIn;
    @FXML
    private Hyperlink steamLink;
    private BooleanProperty isLoggingIn = new SimpleBooleanProperty(false);

    private AnchorPane login;
    private ResourceBundle resource;

    /**
     * Screen for the Login
     */
    public LoginScreen() {
        super();

        // Load xml
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/monkygames/nautdrafter/view/Login.fxml"), resource = NautDrafter.getBundle());

        fxmlLoader.setController(this);

        try {
            login = fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        // Player sign in button
        this.playerSignIn.setOnAction((ActionEvent e) -> {

            System.out.println("User clicked \"Sign in as player\" button");

            NautDrafter.getBase().startLoadingOverlay(resource.getString("login.loading"));

            // Start the logging in process
            LobbyClient.login(this);
        });

        // Specatator sign in button
        this.spectatorSignIn.setOnAction((ActionEvent e) -> {
            System.out.println("User clicked \"Continue as spectator\" button");
            showNextScreen(new ServerBrowserScreen(false));
           
            NautDrafter.getBase().setPlayer(new Player("Spectator", "/assets/ronimo/images/icons/logo.png"));
        });

        // Steam url hyperlink
        this.steamLink.setOnAction((ActionEvent e) -> {
            NautDrafter.getInstance().startSteam();
        });
    }

    /**
     * Set the player details then progress to next screen
     * @param player
     */
    @Override
    public void onLoginDetailsReturn(Player player) {

        if (Platform.isFxApplicationThread()) {

            NautDrafter.getBase().stopLoadingOverlay();

            if (player == null) {
                System.out.println("Failed Login");
                new PopUp(resource.getString("login.error"), resource.getString("login.error.title")).showPopup();
                return;
            }

            System.out.println("LOGIN DETAILS RETURNED IN CALLBACK, name = " + player.name + ", url = " + player.iconUrl);
            NautDrafter.getBase().setPlayer(player);
            showNextScreen(new ServerBrowserScreen(true));

        } else {
            Platform.runLater(() -> this.onLoginDetailsReturn(player));
        }
    }

    private void showNextScreen(SubScreen screen) {
        NautDrafter.getBase().setTitleAreaVisable(true);
        NautDrafter.getInstance().resizeScreen(1080, 600, true);
        NautDrafter.setScreen(screen, true);
        
    }

    @Override
    public Node getRoot() {
        return login;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public void onShow() {
        Platform.runLater(()->NautDrafter.getBase().stopLoadingOverlay());
    }

}
