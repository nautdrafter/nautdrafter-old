/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.view.ImageCacher;
import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

/**
 *
 * @author Adam
 */
public class TeamPicker extends VBox {

    private TextField teamNameField;

    private TeamSelectionToggle captainButton;

    private TeamSelectionToggle crewButton1;

    private TeamSelectionToggle crewButton2;

    private ToggleGroup group;
    public static final int RED = 1, BLUE = 2;
    private static Background RED_BG = new Background(new BackgroundFill(new Color(1, 0, 0, 1), CornerRadii.EMPTY, Insets.EMPTY));
    private static Background BLUE_BG = new Background(new BackgroundFill(new Color(0, 0, 1, 1), CornerRadii.EMPTY, Insets.EMPTY));

    public TeamPicker() {
        HBox textFieldContainer = new HBox(teamNameField = new TextField());
        teamNameField.setOnAction(action -> {
            String text;
            if ((text = teamNameField.getText()) == null || text.isEmpty()) {
                return;
            }              
            textFieldContainer.requestFocus();
        });
        teamNameField.focusedProperty().addListener((ov,oldv,newv)->{
            if(!newv){
                System.out.println((teamProperty().get()==RED?"Red":"Blue") +" team changed name to "+teamNameField.getText() );
                //Fire name change event
            }
        });
        textFieldContainer.setAlignment(Pos.CENTER);
        VBox.setVgrow(textFieldContainer, Priority.ALWAYS);
        VBox captainContainer = new VBox(new Label(NautDrafter.getBundle().getString("team_picker.captain")), this.captainButton = new TeamSelectionToggle(TeamSelectionScreen.ValidSelectionListner.CAPTAIN));
        this.captainButton.setPrefSize(64, 64);
        captainContainer.setAlignment(Pos.CENTER);
        VBox.setVgrow(captainContainer, Priority.ALWAYS);
        HBox crewIconsBox = new HBox(this.crewButton1 = new TeamSelectionToggle(TeamSelectionScreen.ValidSelectionListner.CREW_1), this.crewButton2 = new TeamSelectionToggle(TeamSelectionScreen.ValidSelectionListner.CREW_2));
        crewIconsBox.setAlignment(Pos.CENTER);
        crewIconsBox.setStyle("-fx-spacing: 16px;");
        this.crewButton1.setPrefSize(64, 64);
        this.crewButton2.setPrefSize(64, 64);
        VBox crewContainer = new VBox(new Label(NautDrafter.getBundle().getString("team_picker.crew")), crewIconsBox);        
        crewContainer.setAlignment(Pos.TOP_CENTER);
        VBox.setVgrow(crewContainer, Priority.ALWAYS);
        this.getChildren().addAll(textFieldContainer, captainContainer, crewContainer);

        this.captainButton.setIcon(ImageCacher.get("empty"));
        this.crewButton1.setIcon(ImageCacher.get("empty"));
        this.crewButton2.setIcon(ImageCacher.get("empty"));

        this.group = new ToggleGroup();

        this.captainButton.setToggleGroup(this.group );
        this.crewButton1.setToggleGroup(this.group );
        this.crewButton2.setToggleGroup(this.group );

        

    }

    public void setToBeUnique(TeamPicker other){
        this.captainButton.setOtherToggleGroup(other.group );
        this.crewButton1.setOtherToggleGroup(other.group);
        this.crewButton2.setOtherToggleGroup(other.group );
        other.captainButton.setOtherToggleGroup(this.group );
        other.crewButton1.setOtherToggleGroup(this.group);
        other.crewButton2.setOtherToggleGroup(this.group );
    }
    private IntegerProperty team;

    public final IntegerProperty teamProperty() {
        if (team == null) {
            team = new SimpleIntegerProperty() {

                @Override
                public Object getBean() {
                    return TeamPicker.this;
                }

                @Override
                public String getName() {
                    return "team";
                }

                @Override
                public void invalidated() {
                    Platform.runLater(() -> {
                        teamNameField.setPromptText(getTeamName());
                        getStyleClass().remove(get() == RED ? "blue-team" : "red-team");
                        getStyleClass().add(get() == RED ? "red-team" : "blue-team");
                    });
                }

            };
        }
        return team;
    }

    public final void setTeam(int value) {
        teamProperty().set(value);
    }

    public final int getTeam() {
        return team == null ? 0 : team.get();
    }

    public final String getTeamName() {
        switch (getTeam()) {
        case BLUE:
            return NautDrafter.getBundle().getString("team_picker.blue");
        case RED:
            return NautDrafter.getBundle().getString("team_picker.red");
        default:
            return "Team not defined";
        }
    }   
    
    public void setValidSelectionListner(TeamSelectionScreen.ValidSelectionListner listner){        
        this.captainButton.setValidSelectionListner(listner);
        this.crewButton1.setValidSelectionListner(listner);
        this.crewButton2.setValidSelectionListner(listner);
    }
    
    private TeamSelectionScreen.NameChangeListner nameChanger;
    public void setNameChangeListner(TeamSelectionScreen.NameChangeListner listner){        
       nameChanger = listner;
    }
    
    
}
