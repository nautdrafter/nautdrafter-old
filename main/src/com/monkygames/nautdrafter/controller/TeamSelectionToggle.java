/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.view.ImageCacher;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.ActionEvent;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Adam
 */
public class TeamSelectionToggle extends ToggleButton {

    private ImageView icon;
    private long player;
    private ToggleGroup other;
    private int position;

    public TeamSelectionToggle(int position) {
        super();
        this.position = position;
        icon = new ImageView();
        setGraphic(icon);
        icon.setFitHeight(64);
        icon.setFitWidth(64);

        this.selectedProperty().addListener((observable, oldValue, newValue) -> {
            //if ((newValue == null || (oldValue && !newValue)) && (getToggleGroup().getSelectedToggle() != null)) {
            //    setIsPlayerSet(false);
            //}
        });

        this.getStyleClass().addAll("toggle-me");
       
    }

    public void setOtherToggleGroup(ToggleGroup other) {
        this.other = other;
    }

    public void setPlayer(long id, Image i) {
        icon.setImage(i);
        player = id;
        setIsPlayerSet(true);
    }

    public void deselect() {
        setIsPlayerSet(false);
    }

    public void setIcon(Image i) {
        icon.setImage(i);
    }

    @Override
    public void fire() {
        long id = NautDrafter.getBase().getPlayer().steamId;

        if (alreadyInOtherTeam(id)) {
            System.out.println("Cancle slection: already in the other team");
            return;
        }
        if (getIsPlayerSet() && id != this.player) {
            System.out.println("Cancle slection: another player already there");
            return;
        }

        if (alreadyInThisTeam(id) && id != this.player) {
            System.out.println("Cancle slection: already player in this team");
            return;
        }

        if (this.listner != null) {
            this.listner.onValidSelection(position, player == 0);
        }

        //Until networking is done
        if (player == 0) {
            setPlayer(id, ImageCacher.get("player"));
        } else {
            deselect();
        }
        if (!isDisabled()) {
            setSelected(!isSelected());
            fireEvent(new ActionEvent());
        }
    }

    private BooleanProperty setPlayer;

    public final BooleanProperty setPlayerProperty() {
        if (setPlayer == null) {
            setPlayer = new SimpleBooleanProperty() {

                @Override
                public Object getBean() {
                    return TeamSelectionToggle.this;
                }

                @Override
                public String getName() {
                    return "team";
                }

                @Override
                public void invalidated() {
                    Platform.runLater(() -> {
                        if (!setPlayer.get()) {
                            icon.setImage(ImageCacher.get("empty"));
                            player = 0;
                        }
                    });
                }
            };
        }
        return setPlayer;
    }

    private boolean setIsPlayerSet(boolean value) {
        if (value && getIsPlayerSet()) {
            return false;
        }
        setPlayerProperty().set(value);
        return true;
    }

    public final boolean getIsPlayerSet() {
        return setPlayer == null ? false : setPlayer.get();
    }

    private boolean alreadyInOtherTeam(long id) {
        return this.other.getToggles().stream().anyMatch((item) -> (((TeamSelectionToggle) item).player == id));
    }

    private boolean alreadyInThisTeam(long id) {
        return this.getToggleGroup().getToggles().stream().anyMatch((item) -> (((TeamSelectionToggle) item).player == id));
    }

    private TeamSelectionScreen.ValidSelectionListner listner;

    public void setValidSelectionListner(TeamSelectionScreen.ValidSelectionListner l) {
        this.listner = l;
    }
}
