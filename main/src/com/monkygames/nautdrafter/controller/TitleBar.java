/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.view.ImageCacher;
import java.io.IOException;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

/**
 * Pane to show at the top of the Base class for player details and things
 * @author Adam
 */
public class TitleBar extends HBox {

    @FXML
    private Label screenName;
    
    @FXML
    private Label playerName;
    @FXML
    private ImageView playerIcon;

    private Player player;
    
    @FXML
    private HBox logoContainer;
    

    /**
     * Construct a new instance of TitleBar by loading FXML from file
     */
    public TitleBar() {
        super();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/monkygames/nautdrafter/view/TitleBar.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }   
    }
    

    /**
     * Set screens name. Only Base should call this
     *
     * @param title
     */
    void setScreenName(String title) {
        screenName.setText(title);
    }

    /**
     * Set players name
     *
     * @param player
     */
    public void setPlayer(Player player) {
        this.player = player;
        this.playerName.setText(this.player.name);
        this.playerIcon.setImage(ImageCacher.get(this.player.iconUrl));
    }

    /**
     * Get the player
     * @return The player
     */
    public Player getPlayer() {
        return this.player;
    }
    
    /**
     * Get the image of the icon
     * @return 
     */
    public Image getPlayerIcon(){
        return this.playerIcon.getImage();
    }
    
    /**
     * Set game logo
     *
     * @param image
     */
    public void setApplicationLogo(ImageView image) {
        logoContainer.getChildren().clear();
        logoContainer.getChildren().add(image);
    }
}
