/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.drafting.network.DraftingServer;
import com.monkygames.nautdrafter.view.ImageCacher;
import com.monkygames.nautdrafter.view.SubScreen;
import java.io.IOException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.binding.Bindings;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author Adam
 */
public class ServerConfigScreen extends VBox implements SubScreen {

    @FXML
    private Label passwordLabel;
    @FXML
    private PasswordField passwordField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField detailsField;
    @FXML
    private CheckBox passwordCheckBox;
    @FXML
    private ChoiceBox<String> mapMenu;
    @FXML
    private ImageView mapImage;
    @FXML
    private ImageView mapImageMap;
    @FXML
    private Button cancelButton;
    @FXML

    private Button createButton;
    private final String title;
    private StringProperty finalDetails = new SimpleStringProperty("");
    private StringProperty finalName = new SimpleStringProperty();
    private StringProperty finalPassword = new SimpleStringProperty();
    ;
    private BooleanProperty isPasswordProtected = new SimpleBooleanProperty(false);
    ;
    private int map;

    public ServerConfigScreen() {
        // Load xml
        ResourceBundle resource;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/monkygames/nautdrafter/view/ServerConfig.fxml"), resource = NautDrafter.getBundle());
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }
        this.title = resource.getString("server_config.screen_name");
        this.mapMenu.getItems().addAll("Ribbit IV", "AI Station 404", "Sorona", "Aiguillon");
        this.mapMenu.getSelectionModel().selectedIndexProperty().addListener((ov, oldVal, newVal) -> {
            System.out.println("Selected map " + newVal.intValue());
            this.mapImage.setImage(ImageCacher.get("m" + newVal.intValue() + "l"));
            this.mapImageMap.setImage(ImageCacher.get("m" + newVal.intValue() + "m"));
            this.map = newVal.intValue();
        });

        this.mapMenu.getSelectionModel().select(0);
        this.passwordField.disableProperty().bind(Bindings.not(this.passwordCheckBox.selectedProperty()));
        this.passwordLabel.disableProperty().bind(Bindings.not(this.passwordCheckBox.selectedProperty()));
        this.cancelButton.setOnAction((ActionEvent e) -> {
            NautDrafter.getBase().popMain();
        });

        this.finalDetails.bind(this.detailsField.textProperty());
        this.finalName.bind(this.nameField.textProperty());
        this.isPasswordProtected.bind(this.passwordCheckBox.selectedProperty());
        this.finalPassword.bind(this.passwordField.textProperty());

        this.createButton.setOnAction((ActionEvent e) -> {

            NautDrafter.getBase().startLoadingOverlay("Starting server");

            // Create new server
            System.out.println("Create new server:\nName: " + finalName.get() + "\nDetails: " + finalDetails.get());
            System.out.println("is password protected? " + isPasswordProtected.get() + " " + (isPasswordProtected.get() ? ", password = " + finalPassword.get() : ""));
            System.out.println("map id: " + map);

            NautDrafter.getBase().startLoadingOverlay("Creating server");

            new Thread(() -> {
                DraftingServer newServer;
                try {
                    newServer = new DraftingServer(this.finalName.get(), this.finalDetails.get(), this.isPasswordProtected.get() ? this.finalPassword.get() : null, true);
                    new Thread(() -> newServer.run()).start();
                    NautDrafter.getInstance().addNewDraftingServer(newServer);
                } catch (IOException ex) {
                    Logger.getLogger(ServerConfigScreen.class.getName()).log(Level.SEVERE, null, ex);
                }
                this.onServerCreated();
            }).start();

        });

        this.createButton.setDisable(true);

        this.nameField.textProperty().addListener((ov, oldVal, newVal) -> {
            if (newVal == null || newVal.isEmpty()) {
                this.createButton.setDisable(true);
            } else {
                this.createButton.setDisable(false);
            }
        });
    }

    @Override
    public Node getRoot() {
        return this;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public void onShow() {

    }

    /**
     * Called when server is created
     */
    public void onServerCreated() {
        NautDrafter.getBase().stopLoadingOverlay();
        NautDrafter.getBase().popMain();
    }
}
