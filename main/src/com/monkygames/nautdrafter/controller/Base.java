/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.view.LockerPane;
import com.monkygames.nautdrafter.view.SubScreen;
import java.io.IOException;
import java.util.ArrayDeque;
import java.util.NoSuchElementException;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.Pane;

/**
 * The root node for all screens apart from login
 *
 * @author agkf1
 */
public class Base extends BorderPane {

    private boolean addShowing, titleShowing;
    private Pane ad;
    private SubScreen root;
    private TitleBar title;
    private ArrayDeque<SubScreen> stack;

    @FXML
    private AnchorPane top;
    @FXML
    private AnchorPane right;

    @FXML
    private Base base;

    @FXML
    private LockerPane center;

    {
        stack = new ArrayDeque<>();
    }

    /**
     * Should not really be used
     */
    public Base() {
        super();

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/monkygames/nautdrafter/view/Base.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        title = new TitleBar();
        anchor(title);

        top.getChildren().add(title);
        titleShowing = true;

    }

    public SubScreen getCurrentScreen() {
        return root;
    }

    /**
     * Switch main area so the designated screen but does not save last screen on the stack
     *
     * @param screen
     */
    public void setMain(SubScreen screen) {
        root = screen;
        setRootScreen(root);
    }

    /**
     * Switch main area so the designated screen and saves last screen on the stack
     *
     * @param screen
     */
    public void addMain(SubScreen screen) {
        stack.addFirst(root);
        root = screen;
        setRootScreen(root);
    }

    /**
     * Return to last saved screen on the stack If nothing is on stack and exception is thrown
     */
    public void popMain() {
        if (stack.isEmpty()) {
            throw new NoSuchElementException("No screen left on stack");
        }
        root = stack.removeFirst();
        setRootScreen(root);
    }

    // TODO: Need to check if on fx thread before changing ui
    /**
     * Set whether the title bar at the top exists or not
     *
     * @param isVisable
     */
    public void setTitleAreaVisable(boolean isVisable) {
        if (isVisable) {
            //Only bother showing if root exists and if not showing
            if (root != null && !titleShowing) {
                titleShowing = true;
                title.setScreenName(root.getTitle());
                setTop(top);
            }
        } else {
            // Remove title bar by setting top to null
            titleShowing = false;
            setTop(null);
        }
    }

    /**
     * Set a node to the ad area
     *
     * @param ad
     */
    public void setAdArea(Pane ad) {
        anchor(ad);
        this.ad = ad;
        resizeImageViews(ad);
        // Only show if needed
        if (addShowing) {
            right.getChildren().clear();
            right.getChildren().add(ad);
        }

    }

    /**
     * Set visibility of the ads
     *
     * @param isVisable
     * @return whether the ads were shown/exist
     */
    public boolean setAdVisable(boolean isVisable) {
        if (isVisable) {
            if (ad != null && !addShowing) {
                addShowing = true;
                setRight(right);
                right.getChildren().clear();
                right.getChildren().add(ad);
                return true;
            }
        } else {
            addShowing = false;
            setRight(null);
            right.getChildren().clear();
        }
        return false;
    }

    /**
     * Set the player in the title bar
     *
     * @param player
     */
    public void setPlayer(Player player) {
        title.setPlayer(player);
    }

    /**
     * Get the player
     *
     * @return The player
     */
    public Player getPlayer() {
        return title.getPlayer();
    }
    
    /**
     * Set the game logo in the title bar
     *VBox
     * @param logo
     */
    public void setAppIcon(Image logo) {
        title.setApplicationLogo(new ImageView(logo));
    }

    /**
     * Start the overlay. Disables all UI
     *
     * @param content String to display
     */
    public void startLoadingOverlay(String content) {
        if (Platform.isFxApplicationThread()) {
            System.out.println("Starting loading overlay");
           center.lock(content);
        } else {
            Platform.runLater(() -> startLoadingOverlay(content));
        }
    }

    /**
     * Stop the overlay. Re-enables the UI
     */
    public void stopLoadingOverlay() {
        if (Platform.isFxApplicationThread()) {
            System.out.println("Stopping loading overlay");
            center.unlock();
        } else {
            Platform.runLater(() -> stopLoadingOverlay());
        }
    }

    /**
     * Set the screen to display. Checks if on the fx thread to not cause errors
     *
     * @param screen
     */
    private void setRootScreen(SubScreen screen) {

        if (Platform.isFxApplicationThread()) {
            Node rootNode = screen.getRoot();
            anchor(rootNode);
            center.setRootContent(rootNode);
            screen.onShow();
            title.setScreenName(root.getTitle());
        } else {
            Platform.runLater(() -> setRootScreen(screen));
        }
    }

    /**
     * Set the node to be expanded in the anchor pane
     *
     * @param node
     */
    private void anchor(Node node) {
        AnchorPane.setBottomAnchor(node, 0d);
        AnchorPane.setTopAnchor(node, 0d);
        AnchorPane.setRightAnchor(node, 0d);
        AnchorPane.setLeftAnchor(node, 0d);
    }

    private void resizeImageViews(Parent n) {
        for (Node child : n.getChildrenUnmodifiable()) {
            if (child instanceof Parent) {
                resizeImageViews((Parent) child);
            } else if (child instanceof ImageView) {
                ImageView i;
                if ((i = (ImageView) child).getLayoutBounds().getWidth() > 256) {
                    System.out.println("Resized image " + i.toString() + " to fit 256px wide");
                    i.setFitWidth(256);
                    i.setPreserveRatio(true);
                }
            }

        }
    }
}
