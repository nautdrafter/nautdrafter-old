/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.controller;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.common.network.ServerInfo;
import com.monkygames.nautdrafter.drafting.network.callbacks.ConnectionCallback;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;
import com.monkygames.nautdrafter.drafting.network.callbacks.ServerBrowserCallback;
import com.monkygames.nautdrafter.network.LobbyClient;
import com.monkygames.nautdrafter.view.ImageCacher;
import com.monkygames.nautdrafter.view.ServerItem;
import com.monkygames.nautdrafter.view.SubScreen;
import java.io.IOException;
import java.net.Socket;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContentDisplay;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 *
 * @author Adam
 */
public class ServerBrowserScreen extends VBox implements SubScreen, ServerBrowserCallback, ConnectionCallback {

    private final boolean isPlayer;
    private Button joinButton, spectateButton;
    private ProgressIndicator refreshSpinner;
    private final String screenName;

    @FXML
    private TableView<ServerItem> tableView;
    @FXML
    private TableColumn<ServerItem, ServerItem> serverColumn;
    @FXML
    private TableColumn<ServerItem, String> playerColumn;
    @FXML
    private TableColumn<ServerItem, Image> lockColumn;

    @FXML
    private Button refreshButton;

    @FXML
    private HBox rightButtons;

    @FXML
    private TextField filterField;

    private final ObservableList<ServerItem> serverData;

    {
        serverData = FXCollections.observableArrayList();
    }

    ServerItem currentSelected;
    // Testing
    public static Thread t;

    /**
     * Create the screen using different layouts for player/spectator
     *
     * @param playerOrNot Set true for player and false for spectator
     */
    public ServerBrowserScreen(boolean playerOrNot) {

        // Disable Ads for this screen
        NautDrafter.getBase().setAdVisable(false);

        // Load xml
        ResourceBundle resource;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/com/monkygames/nautdrafter/view/ServerBrowserScreen.fxml"), resource = NautDrafter.getBundle());
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        // Add listner for selection changes in table
        this.tableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> {
                    if (this.currentSelected == null) {
                        this.currentSelected = newValue;
                    }
                    boolean setOrDelselect = (newValue == null);

                    if (oldValue != null && newValue != null) {
                        this.currentSelected = newValue;
                    }
                    System.out.println(newValue == null ? "DESELECTED" : "SELECTED " + newValue.getServerName());
                    // Disabl/enable buttons depending on what happens
                    this.spectateButton.setDisable(setOrDelselect);
                    if (playerOrNot) {
                        this.joinButton.setDisable(setOrDelselect);
                    }

                });

        // Set up columns
        // First column
        // Sorting comparator used for sorting by name of the server
        this.serverColumn.setComparator(Comparator.comparing(ServerItem::getServerName));

        // Node display
        this.serverColumn.setCellValueFactory((CellDataFeatures<ServerItem, ServerItem> p) -> p.getValue().observable);
        this.serverColumn.setCellFactory((TableColumn<ServerItem, ServerItem> p) -> new TableCell<ServerItem, ServerItem>() {
            final ImageView map = new ImageView();
            final HBox main;
            final Label name;
            final Label details;

            // Set up node structure
            {
                this.main = new HBox();
                this.main.setPadding(new Insets(8));
                this.setAlignment(Pos.CENTER_LEFT);
                VBox.setVgrow(this, Priority.NEVER);

                // Icon of map
                this.map.setFitHeight(48);
                this.map.setFitWidth(48);
                HBox.setHgrow(this.map, Priority.NEVER);

                // Details area properties
                VBox detailsBox = new VBox();
                detailsBox.setPadding(new Insets(0, 0, 0, 8));
                detailsBox.setAlignment(Pos.CENTER_LEFT);
                HBox.setHgrow(detailsBox, Priority.ALWAYS);
                detailsBox.getChildren().addAll(name = new Label(), this.details = new Label());
                this.details.setMinWidth(USE_PREF_SIZE);
                this.name.setMinWidth(USE_PREF_SIZE);
                this.main.getChildren().addAll(this.map, detailsBox);
            }

            @Override
            public void updateItem(final ServerItem source, boolean empty) {
                super.updateItem(source, empty);

                if (!empty) {

                    // if not empty display the main HBox and fill in the details of the fields
                    this.name.setText(source.getServerName());
                    this.details.setText(source.getServerDetails());
                    this.map.setImage(ImageCacher.get("m" + source.getMapId() + "s"));
                    this.setGraphic(this.main);
                } else {
                    this.setGraphic(null);
                }
            }
        });

        //Player count column, data already comes as needed to display
        this.playerColumn.setCellValueFactory(cellData -> cellData.getValue().playerCount());

        this.playerColumn.setCellFactory((TableColumn<ServerItem, String> p) -> new TableCell<ServerItem, String>() {
            final VBox vb;
            final Label l;

            {
                this.vb = new VBox(this.l = new Label());
                this.vb.setAlignment(Pos.CENTER_LEFT);
            }

            @Override
            public void updateItem(final String source, boolean empty) {
                super.updateItem(source, empty);
                if (!empty) {
                    l.setText(source);
                    this.setGraphic(this.vb);
                } else {
                    this.setGraphic(null);
                }
            }
        });

        // Lock display column
        // Make the value returned the icon of the lockStatus
        this.lockColumn.setCellValueFactory((CellDataFeatures<ServerItem, Image> p) -> p.getValue().lockStatus());

        // Node display
        this.lockColumn.setCellFactory((TableColumn<ServerItem, Image> p) -> new TableCell<ServerItem, Image>() {
            final ImageView v;
            final VBox vb;

            {
                // Creating and alligning the imageView
                this.v = new ImageView();
                this.vb = new VBox(this.v);
                this.vb.setAlignment(Pos.CENTER_LEFT);
            }

            @Override
            public void updateItem(final Image source, boolean empty) {
                super.updateItem(source, empty);
                if (!empty) {
                    this.v.setImage(source);
                    this.setGraphic(vb);
                } else {
                    this.setGraphic(null);
                }
            }
        });

        // For searching servers
        FilteredList<ServerItem> filteredData = new FilteredList<>(this.serverData, p -> true);

        // Set the filter whenever the text field changes.
        this.filterField.textProperty().addListener((observable, oldValue, newValue) -> {
            filteredData.setPredicate(server -> {

                // If filter text is empty, display all persons.
                if (newValue == null || newValue.isEmpty()) {
                    return true;
                }

                // Compare server name and details against text field
                String lowerCaseFilter = newValue.toLowerCase();
                return (server.getServerName().toLowerCase().contains(lowerCaseFilter) || server.getServerDetails().toLowerCase().contains(lowerCaseFilter));

            });
        });

        // Add sortinglist to table and bind comparators
        SortedList<ServerItem> sortedList = new SortedList<>(filteredData);
        sortedList.comparatorProperty().bind(this.tableView.comparatorProperty());

        // Set the items in the table
        this.tableView.setItems(sortedList);

        // Set the placeholder
        VBox placeholder = new VBox(new Label(playerOrNot ? resource.getString("server_browser.placeholder.player") : resource.getString("server_browser.placeholder.spectator")));
        placeholder.setAlignment(Pos.CENTER);
        this.tableView.setPlaceholder(placeholder);

        // Sort out active item when re sorting
        this.tableView.getItems().addListener((ListChangeListener.Change<? extends ServerItem> c) -> {
            if (this.currentSelected == null) {
                return;
            }
            ServerItem test = this.tableView.getSelectionModel().getSelectedItem();
            if (test == null) {
                return;
            }
            if (!test.equals(this.currentSelected)) {
                this.tableView.getSelectionModel().select(this.currentSelected);
            }
        });

        // Set screen properties
        this.screenName = resource.getString("server_browser.screen_name");
        this.isPlayer = playerOrNot;
        this.setButtons(resource);

        // Regester callback
        DraftingCallbacks.serverBrowserCallback = this;
        DraftingCallbacks.connectionCallback = this;
        this.refreshButton.setDisable(true);

    }

    @Override
    public Node getRoot() {
        return this;
    }

    @Override
    public String getTitle() {
        return this.screenName;
    }

    @Override
    public void onShow() {
        LobbyClient.getServerList();
    }

    /**
     * Returns the data as an observable list of ServerItem.
     *
     * @return
     */
    public ObservableList<ServerItem> getServerData() {
        return this.serverData;
    }

    private void setButtons(ResourceBundle resource) {

        // Player only buttons
        if (this.isPlayer) {
            Button host = new Button(resource.getString("server_browser.host"));
            this.joinButton = new Button(resource.getString("server_browser.join_player"));

            this.rightButtons.getChildren().add(host);

            host.setOnAction((ActionEvent e) -> {
                // Create new Server
                System.out.println("USER clicked on \"Host new game\"");
                NautDrafter.getBase().addMain(new ServerConfigScreen());
            });
            host.setPrefWidth(166.0);

            this.joinButton.setOnAction((ActionEvent e) -> {
                // Join Server
                System.out.println("USER clicked on \"Join game\". We should now join sevrer: " + tableView.getSelectionModel().getSelectedItem().getServerName());

                NautDrafter.getBase().startLoadingOverlay("Connecting to server");

                ServerItem server = this.tableView.getSelectionModel().getSelectedItem();

                LobbyClient.connectTo(server.getAddress(), server.getPort(), NautDrafter.getBase().getPlayer());
            });
            this.joinButton.setPrefWidth(166.0);
            this.joinButton.setDisable(true);
        }

        // Buttons for everyone
        this.spectateButton = new Button(resource.getString("server_browser.join_spectator"));

        if (this.isPlayer) {
            this.rightButtons.getChildren().addAll(this.spectateButton, this.joinButton);
        } else {
            this.rightButtons.getChildren().add(this.spectateButton);
        }

        this.spectateButton.setOnAction((ActionEvent e) -> {
            // Spectate game
            System.out.println("USER clicked on \"Join as spectator\". We should spectate sevrer: " + tableView.getSelectionModel().getSelectedItem().getServerName());

            NautDrafter.getBase().startLoadingOverlay("Connecting to server");

            ServerItem server = this.tableView.getSelectionModel().getSelectedItem();
            LobbyClient.connectTo(server.getAddress(), server.getPort(), null);
            
            NautDrafter.getBase().addMain(new TeamSelectionScreen());
        });

        this.spectateButton.setPrefWidth(166.0);
        this.spectateButton.setDisable(true);

        this.refreshButton.setOnAction((ActionEvent e) -> {
            // Look for new servers
            System.out.println("USER clicked on \"Refresh\"");
            this.refreshButton.setDisable(true);
            this.refreshSpinner.setVisible(true);
            LobbyClient.getServerList();
        });

        this.refreshSpinner = new ProgressIndicator();
        this.refreshSpinner.setProgress(ProgressIndicator.INDETERMINATE_PROGRESS);
        this.refreshSpinner.setMaxSize(40, 40);
        VBox cont = new VBox(refreshSpinner);
        cont.setMaxHeight(40);
        cont.setAlignment(Pos.CENTER_RIGHT);
        cont.setPadding(new Insets(-20, -40, -20, 0));
        this.refreshSpinner.setVisible(true);
        this.refreshButton.setContentDisplay(ContentDisplay.RIGHT);
        this.refreshButton.setGraphic(cont);
    }

    /**
     * Adds the servers from the callback into the tableView and enables the refresh button
     *
     * @param servers
     */
    @Override
    public void addServers(List<ServerInfo> servers) {

        if (!Platform.isFxApplicationThread()) {
            Platform.runLater(() -> this.addServers(servers));
            return;
        }        

        this.refreshButton.setDisable(false);
        this.refreshSpinner.setVisible(false);
        if (servers == null || servers.isEmpty()) {
            System.out.println("No servers");
            this.serverData.clear();
            return;
        }

        // Save refrence to the currently selected item
        ServerItem selected = this.serverData.size() == 0 ? null : this.tableView.getSelectionModel().getSelectedItem();

        // Refresh list of servers
        this.serverData.clear();
        servers.forEach(server -> this.serverData.add(new ServerItem(server)));

        // If selected item was deleted then deselect any selection
        if (!this.serverData.contains(selected)) {
            this.tableView.getSelectionModel().clearSelection();
            this.currentSelected = null;
            return;
        }

        // Reset selected item
        this.tableView.getSelectionModel().select(selected);
        this.tableView.scrollTo(tableView.getSelectionModel().getSelectedIndex());
    }

    @Override
    public void serverStarted(boolean success) {
        NautDrafter.getBase().stopLoadingOverlay();
        // TODO: can trigger refresh from here
    }

    @Override
    public void onConnected(Socket s) {
        System.out.println("Connected!!!");
        NautDrafter.getBase().stopLoadingOverlay();
        // TODO: Actually use the connection somewhere
        // For now we just show the Team Selection screen
        NautDrafter.getBase().addMain(new TeamSelectionScreen());
    }
}
