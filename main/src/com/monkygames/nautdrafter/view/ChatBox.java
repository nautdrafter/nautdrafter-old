/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import com.monkygames.nautdrafter.NautDrafter;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;

/**
 * Re usable area for chat
 *
 * @author agkf1
 */
public class ChatBox extends VBox {

    private final TextFlow textArea;
    private final TextField textInput;
    private final ScrollPane textFlowArea;

    /**
     * Create a new instance of the chat
     */
    public ChatBox() {
        textFlowArea = new ScrollPane(textArea = new TextFlow());
        textFlowArea.setFitToWidth(true);
        VBox.setVgrow(textFlowArea, Priority.ALWAYS);
        textInput = new TextField();
        textInput.setPromptText(NautDrafter.getBundle().getString("chat.prompt"));
        VBox.setVgrow(textInput, Priority.NEVER);
        this.getChildren().addAll(textFlowArea, textInput);

        textInput.setOnAction(action -> {
            String text;
            if ((text = textInput.getText()) == null || text.isEmpty()) {
                return;
            }
            textArea.getChildren().add(new Text("["+NautDrafter.getBase().getPlayer().name+"] "+text + "\n"));
            textFlowArea.vvalueProperty().setValue(textFlowArea.getVmax());
            textInput.clear();
            
            //TODO fire message of to server
        });
    }

}
