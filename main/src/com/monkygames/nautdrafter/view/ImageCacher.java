/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import com.monkygames.nautdrafter.NautDrafter;
import java.util.HashMap;
import javafx.scene.image.Image;

/**
 *
 * @author Adam
 */
public class ImageCacher {

    private static HashMap<String, Image> map;

    static {
        map = new HashMap<>();
    }

    public static Image get(String id) {
        switch (id.charAt(0)) {
        case 'm':
            switch (id.charAt(1)) {
            case '0':
                return getAndPutIfNotThere("/assets/ronimo/images/maps/ribbit-" + id.charAt(2) + ".png");
            case '1':
                return getAndPutIfNotThere("/assets/ronimo/images/maps/ai-station-" + id.charAt(2) + ".png");
            case '2':
                return getAndPutIfNotThere("/assets/ronimo/images/maps/sorona-" + id.charAt(2) + ".png");

            case '3':
                return getAndPutIfNotThere("/assets/ronimo/images/maps/aiguillon-" + id.charAt(2) + ".png");

            }

            break;
        case 'e':
            if (id.equals("empty")) {
                return getAndPutIfNotThere("/assets/images/icons/empty-icon.png");
            }
            break;
        
            
        }
        
        if(id.equals("player"))return getAndPutIfNotThere(NautDrafter.getBase().getPlayer().iconUrl);
        
        // Default to url type id
        return getAndPutIfNotThere( id);
    }

    private static Image getAndPutIfNotThere(String url) {
        Image i;
        if ((i = map.get(url)) == null) {
            map.put(url, i = new Image(url));
        }
        return i;
    }
}
