/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import com.monkygames.nautdrafter.common.network.ServerInfo;
import java.net.InetAddress;
import java.util.Arrays;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ObservableValue;
import javafx.scene.image.Image;

/**
 *
 * @author Adam
 */
public class ServerItem  {
    
    private StringProperty name;
    private StringProperty playerCount;
    private int numOfPlayers;
    private BooleanProperty isLocked;
    private int map;
    private String details;
    private ObjectProperty<Image> icon;
    private  InetAddress address;
    private  int port;
    
    static final Image LOCK,UNLOCK;
    static
    {
        LOCK = new Image("/assets/images/icons/locked-16px.png");
        UNLOCK = new Image("/assets/images/icons/unlocked-16px.png");
    }
    
    /**
     * Reference to itself for displaying in tableview
     */
    public final ObservableValue<ServerItem> observable = new SimpleObjectProperty<>(this);
    
    /**
     * Create a new ServerItem
     * @param serverName Name of the server
     * @param details Extra details of the server
     * @param map Id of the map
     * @param lockStatus Whether the server is password protected
     */
    public ServerItem(String serverName ,String details ,int map, boolean lockStatus,int players){
        this.details = details;
        this.map = map;
        name = new SimpleStringProperty(serverName);
        playerCount = new SimpleStringProperty(players+"/6");
        isLocked = new SimpleBooleanProperty(lockStatus);
        icon = new SimpleObjectProperty<>(lockStatus?LOCK:UNLOCK);
    }
    
     public ServerItem(ServerInfo info){
         this(info.name,info.description,info.map_id,info.has_password,info.player_count);
         address = info.address;
         port = info.port;
     }
     
    /**
     * Set the current player count
     * @param count
     */
    public void setPlayerCount(int count) {
        numOfPlayers = count;
        playerCount.setValue(count+"/6");
       
    }
    
    /**
     * Get the current player count
     * @return
     */
    public int getPlayerCount(){
        return numOfPlayers;
    }
    
    /**
     * get the player count property
     * @return
     */
    public StringProperty playerCount(){
        return playerCount;
    }
    
    /**
     * Set the server name
     * @param newName
     */
    public void setServerName(String newName){
        name.setValue(newName);
    }   
    
    /**
     * Get the server name
     * @return
     */
    public String getServerName(){
        return name.get();
    }
    
    /**
     * Get the server name property
     * @return
     */
    public StringProperty serverName(){
        return name;
    }
    
    /**
     * Set the lock status
     * @param lock
     */
    public void setLockStatus(boolean lock){
        isLocked.setValue(lock);
        icon.set(lock?LOCK:UNLOCK);
    }

    /**
     * Get the lock status
     * @return
     */
    public boolean getLocksStatus(){
        return isLocked.get();
    }
    
    /**
     * Get the image associated with the lock status
     * @return
     */
    public ObjectProperty lockStatus(){
        return icon;
    }

    /**
     * Get the details of the server
     * @return
     */
    public String getServerDetails() {
        return details;
    }
    
    /**
     * Get the map id of the server
     * @return
     */
    public int getMapId(){
        return map;
    }
    
    /**
     * Get the port number of the server
     * @return
     */
    public int getPort(){
        return port;
    }
    
    /**
     * Get the IP address of the server
     * @return
     */
    public InetAddress getAddress(){
        return address;
    }

    /**
     * Update server item (currently only player count)
     * @param info Update using this information
     */
    public void update(ServerInfo info) {
        setPlayerCount(info.player_count);
    }
    
     @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }
        
        // Test all fields in ServerInfo/Item except lock status and player count
        if (o instanceof ServerItem) {
            ServerItem item = (ServerItem) o;

            return item.getMapId() == map
                    && item.getServerName().equals(name.get())
                    && item.getServerDetails().equals(details)
                    && item.getPort() == port
                    && ((address == null || item.getAddress() == null) ? true : Arrays.equals(address.getAddress(), item.getAddress().getAddress()));

        } 
        return false;
    }
    
    public String toString(){
        return getServerName();
    }
}
