/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;

/**
 * Pane for using when a locking overlay is to be used
 *
 * @author agkf1
 */
public class LockerPane extends StackPane {

    private final BooleanProperty isOverlayShowing;
    private final StringProperty overlayDescription;

    /**
     * Node that holds the "children"
     */
    protected AnchorPane main;

    private final Label overlayWords;

    private final StackPane overlay;

    /**
     * Create an empty locker pane, default is set to locked to work around a bug in javaFX with the ProgressIndicator
     */
    public LockerPane() {
        this.main = new AnchorPane();
        BorderPane.setAlignment(this.main, Pos.CENTER);
        this.main.setId("main");
        ProgressIndicator spinner = new ProgressIndicator(ProgressIndicator.INDETERMINATE_PROGRESS);
        HBox.setHgrow(spinner, Priority.NEVER);
        HBox toCenter = new HBox(spinner);
        toCenter.setAlignment(Pos.CENTER);
        this.overlayWords = new Label();
        this.overlay = new StackPane(toCenter, this.overlayWords);
        this.overlay.setStyle("-fx-background-color: rgba(0, 0, 32,0.3);");
        this.getChildren().addAll(this.main, this.overlay);
        this.isOverlayShowing = new SimpleBooleanProperty(true);
        this.overlayDescription = new SimpleStringProperty("Loading");
        this.overlay.visibleProperty().bind(this.isOverlayShowing);
        this.main.disableProperty().bind(this.isOverlayShowing);
        this.overlayWords.textProperty().bind(this.overlayDescription);
    }

    /**
     * Set the "child" node to the locker pane
     *
     * @param node
     */
    public void setRootContent(Node node) {
        this.main.getChildren().clear();
        this.main.getChildren().add(node);
    }

    /**
     * Show the locking overlay - disables all UI in main node
     *
     * @param content
     */
    public void lock(String content) {
        this.overlayDescription.set(content);
        this.isOverlayShowing.set(true);
    }

    /**
     * Stop the overlay and unlock the UI
     */
    public void unlock() {
        this.isOverlayShowing.set(false);
    }
}
