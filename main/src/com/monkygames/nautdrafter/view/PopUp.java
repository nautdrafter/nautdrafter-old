/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Adam
 */
public class PopUp extends Stage {

    private BorderPane pane;
    private Label message;
    private Button close;
    HBox buttonBox;

    public PopUp(String message, String title) {

        this.message = new Label(message);
        this.initModality(Modality.APPLICATION_MODAL);
        this.setTitle(title);
        this.pane = new BorderPane();
        this.buttonBox = new HBox(this.close = new Button("OK"));
        this.buttonBox.setAlignment(Pos.CENTER);
        this.close.setOnAction((ActionEvent e) -> close());
        BorderPane.setAlignment(this.buttonBox, Pos.CENTER);
        BorderPane.setAlignment(this.message, Pos.CENTER);
        BorderPane.setMargin(this.message, new Insets(5, 5, 5, 5));
        this.pane.setBottom(this.buttonBox);
        this.pane.setCenter(this.message);
        setScene(new Scene(this.pane));
        setResizable(false);
        sizeToScene();
        centerOnScreen();
        this.pane.getStylesheets().add("/com/monkygames/nautdrafter/view/stylesheets/Base.css");
    }

    public PopUp(String message, String title, String button1, String button2, Runnable event1, Runnable event2) {
        this(message, title);
        this.buttonBox.getChildren().clear();

        Button b = new Button(button1);
        b.setOnAction((ActionEvent e) -> {
            this.close();
            event1.run();
        });

        Button b2 = new Button(button2);
        b2.setOnAction((ActionEvent e) -> {
            this.close();
            event2.run();
        });

        buttonBox.getChildren().addAll(b, b2);

    }

    public PopUp(String message, String title, String button1, Runnable event1) {
        this(message, title);
        this.buttonBox.getChildren().clear();

        Button b = new Button(button1);
        b.setOnAction((ActionEvent e) -> {
            close();
            event1.run();
        });

        this.buttonBox.getChildren().addAll(b);

    }

    public void showPopup() {
        this.showAndWait();
    }
}
