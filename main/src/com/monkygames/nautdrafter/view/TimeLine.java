/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.view;

import com.monkygames.nautdrafter.NautDrafter;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.paint.CycleMethod;
import javafx.scene.paint.LinearGradient;
import javafx.scene.paint.Paint;
import javafx.scene.paint.Stop;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

/**
 * Implementation of the Time line to be used on the Drafting (Spectator) screen
 *
 * @author Adam
 */
public class TimeLine extends HBox {

    /**
     * Constant for top team. Can be used like: TOP + BAN
     */
    public static final int TOP = 0;

    /**
     * Constant for bottom team. Can be used like: BOTTOM + PICK
     */
    public static final int BOTTOM = 1;

    /**
     * Constant for pick. Can be used like: TOP + PICK
     */
    public static final int PICK = 0;

    /**
     * Constant for ban. Can be used like: BOTTOM + BAN
     */
    public static final int BAN = 2;

    /**
     * Convert from nanoseconds to seconds
     */
    public final static long NS_to_S = 1000000000l;

    /**
     * Time allocated to pick before using bonus time
     */
    public final static long MAX_TIME_PICK_S = 30, MAX_TIME_PICK = MAX_TIME_PICK_S * NS_to_S;

    /**
     * Time allocated for bonus time
     */
    public final static long MAX_TIME_BONUS_S = 120, MAX_TIME_BONUS = MAX_TIME_BONUS_S * NS_to_S;

    private static final Paint OLD_BAN = new Color(.2, .2, .2, .5);
    private static final Paint OLD_PICK = new Color(.4, .4, .4, .5);
    private static final Paint CLIP = new Color(.2, .2, .2, .8);
    private static final Paint RED_PICK = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop(0, Color.RED), new Stop(1, new Color(1, 0.5, 0.5, 1)));
    private static final Paint RED_BAN = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop(0, new Color(0.5, 0, 0, 1)), new Stop(1, Color.RED));
    private static final Paint BLUE_PICK = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop(0, new Color(0, .5, 1, 1)), new Stop(1, new Color(.5, .75, 1, 1)));
    private static final Paint BLUE_BAN = new LinearGradient(0, 0, 0, 1, true, CycleMethod.NO_CYCLE, new Stop(0, new Color(0, .25, .5, 1)), new Stop(1, new Color(0, .5, 1, 1)));
    private static final Rectangle CLIP_RECT = new Rectangle(0, -5, 0, 15);

    private long timeLeft, latest, bonusTimeTop, bonusTimeBottom;
    private int position, itemWidth = 48;
    private boolean started;
    private TimeLineItem[] items;
    private Label bonusTop, bonusBottom, countDown, bonusTime;
    private ImageView tournamentIcon;
    private HBox tLine;
    private VBox mid;
    private AnimationTimer timer;
    private int timeLinePadding = 5;
    private VBox bonusTimes;
    private Polygon endArrow;

    public TimeLine() {
        super();

    }

    /**
     * Initialize with data
     *
     * @param order order and data for selections
     */
    public void init(int[] order) {

        ObservableList children = getChildren();

        // Generate bonus time area
        bonusTimes = new VBox(5, bonusTop = new Label("2:00"), bonusTime = new Label("Bonus Time"), bonusBottom = new Label("2:00"));
        bonusTop.setId("red-bonus");
        bonusBottom.setId("blue-bonus");
        // Set default max bonus time
        bonusTimeBottom = bonusTimeTop = MAX_TIME_BONUS;
        bonusTimes.setAlignment(Pos.CENTER);
        bonusTimes.autosize();

        // Add to timeline
        children.add(bonusTimes);

        // HBox for actual time line
        tLine = new HBox(timeLinePadding);
        tLine.setAlignment(Pos.CENTER);

        // Sort out sizing
        tournamentIcon = new ImageView(getTournamentIcon());
        itemWidth = (int) ((getParent().prefWidth(0) - tournamentIcon.getLayoutBounds().getWidth() - bonusTimes.getWidth() - (order.length + 1) * timeLinePadding) * (order.length) / ((order.length + 1) * (order.length + 1)));

        // Create items and add to HBox
        items = new TimeLineItem[order.length];
        for (int i = 0; i < order.length; i++) {
            items[i] = new TimeLineItem(i, order[i]);
        }
        tLine.getChildren().addAll(items);
        tLine.getChildren().add(endArrow = endArrow());

        // Set start countdown 
        timeLeft = MAX_TIME_PICK;

        // Main container for timeline and countdown
        mid = new VBox(5, tLine);
        mid.setAlignment(Pos.CENTER);
        mid.setPadding(new Insets(0, itemWidth / 4, 0, itemWidth));
        children.add(mid);
        children.add(tournamentIcon); //TODO: get proper icon
        HBox.setHgrow(tournamentIcon, Priority.NEVER);
        HBox.setHgrow(mid, Priority.ALWAYS);

        // Set up animator timer
        timer = new AnimationTimer() {
            @Override
            public void handle(long now) {

                // Get time elapsed
                long diff = now - (latest == 0 ? now : latest);
                latest = now;

                // On starting set time diff to 0
                if (!started) {
                    started = true;
                    diff = 0;
                }

                // Eat up elapsed time
                timeLeft -= diff;

                // Have we eaten all of our allocated time?
                if (timeLeft > 0) {

                    // Display time left and translate a bit across
                    countDown.setText(minuteFormat((timeLeft - 1) / NS_to_S + 1));
                    double ratio = 1 - (double) timeLeft / MAX_TIME_PICK;
                    CLIP_RECT.setWidth(itemWidth * ratio);

                    items[position].shapeClip.setClip(CLIP_RECT);
                } else {

                    // Display 0 and eat up bonus
                    countDown.setText("0:00");

                    // Determine which teams bonus is getting eaten
                    if (items[position].dir == TOP) {

                        // Decrease time left
                        bonusTimeTop -= diff;

                        // Need to display formatting correctly
                        bonusTop.setText(minuteFormat((bonusTimeTop) / NS_to_S));
                    } else {
                        bonusTimeBottom -= diff;
                        bonusBottom.setText(minuteFormat((bonusTimeBottom) / NS_to_S));
                    }
                }
            }
        };

        setBackground(new Background(new BackgroundFill(new Color(.1, .1, .1, 1), CornerRadii.EMPTY, Insets.EMPTY)));
        setPositionTo(0);

        // Set text styles
        setTextStyles("-fx-fill: #DDDDDD");

        this.layoutBoundsProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) -> {
            resizeTimeline();
        });
    }

    private void resizeTimeline() {
        itemWidth = (int) ((getLayoutBounds().getWidth() - tournamentIcon.getLayoutBounds().getWidth() - bonusTimes.getWidth() - (items.length + 2) * timeLinePadding));
        itemWidth /= items.length + 5;

        
        mid.setPadding(new Insets(0, itemWidth / 4, 0, itemWidth));

        // Create items and add to HBox
        for (int i = 0; i < items.length; i++) {
            items[i].resizeItem();
        }
        endArrow.getPoints().clear();
        endArrow.getPoints().addAll(0d, 0d, itemWidth - 8d, 0d, itemWidth - 8d, -5d, itemWidth + 0d, 2.5d, itemWidth - 8d, 10d, itemWidth - 8d, 5d, 0d, 5d);
    }

    public void setTextStyles(String style) {
        bonusTop.setStyle(style);
        bonusBottom.setStyle(style);
        countDown.setStyle(style);
        bonusTime.setStyle(style);
    }

    /**
     * Formats number to minute structure
     */
    private String minuteFormat(long l) {

        return String.format("%d:%02d", (int) (l / 60), (int) (l % 60));
    }

    /**
     * Start counting down
     */
    public void start() {

        if (started || position >= items.length) {
            return;
        }

        System.out.println("started");
        timer.start();

    }

    /**
     * Stop counting down
     */
    public void stop() {
        if (!started) {
            return;
        }
        System.out.println("stopped");
        timer.stop();
        started = false;
    }

    /**
     *
     * @param i
     */
    public void setTimeTo(int i) {
        timeLeft = i * NS_to_S;

    }

    /**
     * Set selected item
     *
     * @param i Index of item (from 0)
     */
    public final void setPositionTo(int i) {

        if (i >= items.length) {
            throw new IndexOutOfBoundsException("No item in timeline at index " + i);
        }

        items[position].counter.setVisible(false);

        // Grey out all previous
        for (int j = i - 1; j >= 0; j--) {
            items[j].clearBlur();
            items[j].grayOut();
        }

        // Set selected item and give it a blur
        position = i;
        items[position].setBlur();

        // Sort out time countdown
        countDown = items[position].counter;
        countDown.setVisible(true);
        countDown.setText(minuteFormat((timeLeft - 1) / NS_to_S + 1));

        setTimeTo((int) (MAX_TIME_PICK_S));
    }

    /**
     * Update index by one and sort out formatting
     */
    public void positionUpdate() {
        if (position >= items.length) {
            return;
        }

        countDown.setVisible(false);
        items[position].clearBlur();
        items[position].grayOut();
        position++;

        // Deal with end of timeline if needed else format timeline
        if (position < items.length) {
            items[position].setBlur();
            setTimeTo((int) MAX_TIME_PICK_S);
            countDown = items[position].counter;
            countDown.setVisible(true);
            countDown.setText(minuteFormat((timeLeft - 1) / NS_to_S + 1));
        } else {
            countDown.setVisible(false);
            stop();
        }

    }

    // Shape for end arrow
    private Polygon endArrow() {
        Polygon p = new Polygon(0, 0, itemWidth - 8, 0, itemWidth - 8, -5, itemWidth, 2.5, itemWidth - 8, 10, itemWidth - 8, 5, 0, 5);
        p.setFill(new LinearGradient(0, -5, 0, 12, false, CycleMethod.NO_CYCLE, new Stop(0, Color.GREY), new Stop(1, Color.BLACK)));
        return p;
    }

    private String getTournamentIcon() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Time line item class
     */
    public class TimeLineItem extends VBox {

        private Label text, counter;
        private Polygon shape, shapeClip;
        private Rectangle bgBlur;
        private int dir, type;
        private Color blurP, blurB;
        private LinearGradient pickBlur;
        private LinearGradient banBlur;

        /**
         * Create an item in the time line
         *
         * @param pos
         * @param data
         */
        public TimeLineItem(int pos, int data) {

            dir = data & 1;
            type = data & 2;

            // Determine what if any text to display for the item
            text = pos == 0 ? new Label(type == PICK ? "Pick" : "Ban") : items[pos - 1].type != type ? new Label(type == PICK ? "Pick" : "Ban") : new Label();

            counter = new Label();
            counter.setVisible(false);
            counter.setId("counter");

            // Set up background blur
            bgBlur = new Rectangle(itemWidth, 15);
            bgBlur.setFill(Color.TRANSPARENT);

            VBox topV, midV, bottomV;
            // Create shape and format layout
            if (dir == TOP) {

                // Get up shape
                shape = getUpShape();
                shape.setFill(type == PICK ? RED_PICK : RED_BAN);
                shapeClip = getUpShape();
                blurP = new Color(1, 1, 1, 0.5);
                blurB = new Color(1, 1, 1, 0.5);

                // Add to pane and allign
                StackPane v = new StackPane(shape, shapeClip, bgBlur);
                v.setAlignment(Pos.TOP_CENTER);

                counter.setTranslateY(-counter.getLayoutBounds().getHeight() / 2);

                // Add pane and text to VBox
                getChildren().addAll(topV = new VBox(counter), midV = new VBox(v), bottomV = new VBox(text));

            } else {

                // A bit reversed
                shape = getDownShape();
                shape.setFill(type == PICK ? BLUE_PICK : BLUE_BAN);
                shapeClip = getDownShape();
                blurP = new Color(1, 1, 1, 0.5);
                blurB = new Color(1, 1, 1, 0.5);
                StackPane v = new StackPane(shape, shapeClip, bgBlur);
                v.setAlignment(Pos.BOTTOM_CENTER);
                counter.setTranslateY(counter.getLayoutBounds().getHeight() / 2);

                getChildren().addAll(topV = new VBox(text), midV = new VBox(v), bottomV = new VBox(counter));

            }

            counter.layoutBoundsProperty().addListener((ObservableValue<? extends Bounds> observable, Bounds oldValue, Bounds newValue) -> {
                if (dir == TOP) {
                    Platform.runLater(() -> {
                        bottomV.setMinHeight(counter.getHeight());
                        counter.setTranslateY(-5);
                    });
                } else {
                    Platform.runLater(() -> {
                        topV.setMinHeight(counter.getHeight());
                        counter.setTranslateY(5);
                    });
                }
                System.out.println(counter.getHeight());
            });
            setAlignment(Pos.CENTER);
            topV.setAlignment(Pos.BOTTOM_CENTER);
            midV.setAlignment(Pos.CENTER);
            bottomV.setAlignment(Pos.TOP_CENTER);
            VBox.setVgrow(topV, Priority.ALWAYS);
            VBox.setVgrow(midV, Priority.ALWAYS);
            VBox.setVgrow(bottomV, Priority.ALWAYS);
            shapeClip.setVisible(false);
            pickBlur = new LinearGradient(0, 0, 0, 15, false, CycleMethod.NO_CYCLE, new Stop(0, Color.TRANSPARENT), new Stop(0.5, blurP), new Stop(1, Color.TRANSPARENT));
            banBlur = new LinearGradient(0, 0, 0, 15, false, CycleMethod.NO_CYCLE, new Stop(0, Color.TRANSPARENT), new Stop(0.5, blurB), new Stop(1, Color.TRANSPARENT));

        }

        /**
         * Create the "up" shape
         */
        private Polygon getUpShape() {
            return new Polygon(0, 0, itemWidth / 2 - 4, 0, itemWidth / 2, -5, itemWidth / 2 + 4, 0, itemWidth, 0, itemWidth, 5, 0, 5);
        }

        /**
         * Create the "down" shape
         */
        private Polygon getDownShape() {
            return new Polygon(0, 0, itemWidth, 0, itemWidth, 5, itemWidth / 2 + 4, 5, itemWidth / 2, 10, itemWidth / 2 - 4, 5, 0, 5);
        }

        /**
         * clear blur
         */
        private void clearBlur() {
            bgBlur.setFill(Color.TRANSPARENT);
            shape.setTranslateY(0);
            bgBlur.setTranslateY(0);
            shapeClip.setVisible(false);
            shapeClip.setClip(null);
        }

        /**
         * Create the blur
         */
        private void setBlur() {
            bgBlur.setFill(type == PICK ? pickBlur : banBlur);
            shapeClip.setFill(CLIP);
            CLIP_RECT.setWidth(0);
            shapeClip.setClip(CLIP_RECT);
            shapeClip.setVisible(true);
            shape.setTranslateY(dir == TOP ? -5 : 5);
            bgBlur.setTranslateY(dir == TOP ? -5 : 5);
            shapeClip.setTranslateY(dir == TOP ? -5 : 5);
        }

        /**
         * Grey out item
         */
        private void grayOut() {
            shape.setFill(type == BAN ? OLD_BAN : OLD_PICK);
        }

        private void resizeItem() {
            shape.getPoints().clear();
            shapeClip.getPoints().clear();
            if (dir == TOP) {
                shape.getPoints().addAll(0d, 0d, itemWidth / 2 - 4d, 0d, itemWidth / 2d, -5d, itemWidth / 2 + 4d, 0d, itemWidth + 0d, 0d, itemWidth + 0d, 5d, 0d, 5d);
                shapeClip.getPoints().addAll(0d, 0d, itemWidth / 2 - 4d, 0d, itemWidth / 2d, -5d, itemWidth / 2 + 4d, 0d, itemWidth + 0d, 0d, itemWidth + 0d, 5d, 0d, 5d);
            } else {
                shape.getPoints().addAll(0d, 0d, itemWidth + 0d, 0d, itemWidth + 0d, 5d, itemWidth / 2 + 4d, 5d, itemWidth / 2d, 10d, itemWidth / 2 - 4d, 5d, 0d, 5d);
                shapeClip.getPoints().addAll(0d, 0d, itemWidth + 0d, 0d, itemWidth + 0d, 5d, itemWidth / 2 + 4d, 5d, itemWidth / 2d, 10d, itemWidth / 2 - 4d, 5d, 0d, 5d);
            }
            bgBlur.setWidth(itemWidth);
        }
    }
}
