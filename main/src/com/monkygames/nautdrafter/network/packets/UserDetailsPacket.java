
package com.monkygames.nautdrafter.network.packets;

import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.network.callbacks.NautDrafterCallbacks;

/**
 * This packet will contain all the details needed from steam
 */
public class UserDetailsPacket extends com.monkygames.nautdrafter.common.network.packets.UserDetailsPacket {
    
    public UserDetailsPacket(byte[] data) throws PacketDecoderException {
        super(data);
    }

    @Override
    protected void handle_impl() {
        if (NautDrafterCallbacks.loginDetailsCallback != null) {
            NautDrafterCallbacks.loginDetailsCallback.onLoginDetailsReturn(this.player);
        }
    }
}
