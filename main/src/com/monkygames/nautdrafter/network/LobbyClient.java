/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.network;

import com.monkygames.nautdrafter.NautDrafter;
import com.monkygames.nautdrafter.common.Constants;
import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;
import com.monkygames.nautdrafter.drafting.network.packets.ClientIdPacket;
import com.monkygames.nautdrafter.drafting.network.packets.PlayerJoinedPacket;
import com.monkygames.nautdrafter.drafting.network.packets.ServerListPacket;
import com.monkygames.nautdrafter.network.callbacks.LoginDetailsCallback;
import com.monkygames.nautdrafter.network.packets.UserDetailsPacket;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Provides methods for communicating with the Lobby Server
 * <br>
 * TODO: should probably limit the number of threads spawned, and/or kill off pending requests when a new request is
 * made
 */
public class LobbyClient {

    private LobbyClient() {

    }

    private static class GetServerList extends Thread {

        public GetServerList() {
            super();
        }

        @Override
        public void run() {
            try {
                Socket s = new Socket(InetAddress.getByName(Constants.LOBBY_ADDRESS), Constants.LOBBY_PORT);
                // send request
                s.getOutputStream().write(Packet.SERVER_LIST_REQUEST_PACKET_ID);
                // get response
                byte[] buf = new byte[10000];
                int pos = 0;
                do {
                    int read = s.getInputStream().read(buf, 0, 10000 - pos);
                    if (read == -1) {
                        throw new IOException("Read failed");
                    }
                    pos += read;
                } while (pos == 0);
                if (buf[0] != Packet.SERVER_LIST_PACKET_ID) {
                    throw new IOException("Invalid packet ID");
                }
                ServerListPacket packet = new ServerListPacket(buf);
                while (!packet.decode()) {
                    int read = s.getInputStream().read(buf, 0, 10000 - pos);
                    if (read == -1) {
                        throw new IOException("Read failed");
                    }
                    pos += read;
                }
                // handle
                packet.handle();
                if (DraftingCallbacks.serverBrowserCallback != null) {
                    DraftingCallbacks.serverBrowserCallback.addServers(packet.getServers());
                }
            } catch (IOException ex) {
                if (DraftingCallbacks.serverBrowserCallback != null) {
                    DraftingCallbacks.serverBrowserCallback.addServers(null);
                }
                Logger.getLogger(LobbyClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private static class ConnectTo extends Thread {

        private final InetAddress addr;
        private final int port;
        private final Player playerDetails;

        public ConnectTo(InetAddress addr, int port, Player playerDetails) {
            this.addr = addr;
            this.port = port;
            this.playerDetails = playerDetails;
        }

        @Override
        public void run() {
            try {
                // connect to drafting server
                Socket s = new Socket(this.addr, this.port);
                // handshake with drafting server
                if (this.playerDetails != null) {
                    // join as player
                    s.getOutputStream().write(new PlayerJoinedPacket(this.playerDetails).data);
                } else {
                    // join as spectator
                }
                // callback
                if (DraftingCallbacks.connectionCallback != null) {
                    DraftingCallbacks.connectionCallback.onConnected(s);
                }
            } catch (IOException ex) {
                Logger.getLogger(LobbyClient.class.getName()).log(Level.SEVERE, null, ex);
                if (DraftingCallbacks.connectionCallback != null) {
                    DraftingCallbacks.connectionCallback.onConnected(null);
                }
            }
        }
    }

    private static final String STEAM_AUTH_URL = "https://steamcommunity.com/openid/login/?openid.ns=http://specs.openid.net/auth/2.0&openid.mode=checkid_setup&openid.realm=http://" + Constants.LOBBY_ADDRESS + "&openid.identity=http://specs.openid.net/auth/2.0/identifier_select&openid.claimed_id=http://specs.openid.net/auth/2.0/identifier_select&openid.return_to=http://" + Constants.LOBBY_ADDRESS + ":" + Constants.LOBBY_PORT + "/";

    private static class Login extends Thread {

        LoginDetailsCallback reciever;

        public Login(LoginDetailsCallback reciever) {
            this.reciever = reciever;
        }

        @Override
        public void run() {
            try {
                Socket s = new Socket(InetAddress.getByName(Constants.LOBBY_ADDRESS), Constants.LOBBY_PORT);
                // test for config
                String clientID = NautDrafter.getPreferences().get("clientID", null);
                if (clientID == null) {
                    s.getOutputStream().write(Packet.NEWAUTH_PACKET_ID);
                } else {
                    s.getOutputStream().write((new ClientIdPacket(clientID)).data);
                }
                // get response
                byte[] buf = new byte[10000];
                int pos = 0;
                do {
                    int read = s.getInputStream().read(buf, 0, 10000 - pos);
                    if (read == -1) {
                        throw new IOException("Read failed");
                    }
                    pos += read;
                } while (pos == 0);
                // deal with the ClientID packet
                if (buf[0] == Packet.CLIENTID_PACKET_ID) {
                    ClientIdPacket packet = new ClientIdPacket(buf);
                    while (!packet.decode()) {
                        int read = s.getInputStream().read(buf, 0, 10000 - pos);
                        if (read == -1) {
                            throw new IOException("Read failed");
                        }
                        pos += read;
                    }
                    clientID = packet.getClientId();
                    NautDrafter.getInstance().getHostServices().showDocument(STEAM_AUTH_URL + packet.getClientId());
                    pos = 0;
                    do {
                        int read = s.getInputStream().read(buf, 0, 10000 - pos);
                        if (read == -1) {
                            throw new IOException("Read failed");
                        }
                        pos += read;
                    } while (pos == 0);
                }
                // And now deal with the User Details packet
                if (buf[0] == Packet.USERDETAILS_PACKET_ID) {
                    UserDetailsPacket packet = new UserDetailsPacket(buf);
                    while (!packet.decode()) {
                        int read = s.getInputStream().read(buf, 0, 10000 - pos);
                        if (read == -1) {
                            throw new IOException("Read failed");
                        }
                        pos += read;
                    }
                    NautDrafter.getPreferences().put("clientID", clientID);
                    reciever.onLoginDetailsReturn(packet.getPlayer());
                } else {
                    throw new IOException("FAIL");
                }
            } catch (IOException | IllegalArgumentException ex) {
                reciever.onLoginDetailsReturn(null);
                Logger.getLogger(LobbyClient.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExceptionInInitializerError e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    /**
     * Asks the Lobby Server to give us the current list of Drafting Servers
     */
    public static final void getServerList() {
        new GetServerList().start();
    }

    /**
     * Asks the Lobby Server to help us initiate a connection to a Drafting Server
     *
     * @param addr
     * @param port
     * @param playerDetails
     */
    public static final void connectTo(InetAddress addr, int port, Player playerDetails) {
        new ConnectTo(addr, port, playerDetails).start();
    }

    public static final void login(LoginDetailsCallback reciever) {
        new Login(reciever).start();
    }
}
