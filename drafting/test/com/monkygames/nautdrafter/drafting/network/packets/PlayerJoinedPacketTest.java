/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.common.network.Player;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @see com.monkygames.nautdrafter.network.packets.PlayerJoinedPacket
 */
public class PlayerJoinedPacketTest {

    public PlayerJoinedPacketTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encoding and decoding, of class PlayerJoinedPacket
     */
    @Test
    public void testPacket() throws Exception {
        Random rng = new Random();
        Player player = new Player(rng.nextLong(), "Player", "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg");
        PlayerJoinedPacket encodePacket = new PlayerJoinedPacket(player);
        PlayerJoinedPacket decodePacket = new PlayerJoinedPacket(encodePacket.data, null);
        decodePacket.decode();
        assertEquals(player.steamId, decodePacket.getPlayer().steamId);
        assertEquals(player.name, decodePacket.getPlayer().name);
        assertEquals(player.iconUrl, decodePacket.getPlayer().iconUrl);
    }

    /**
     * Test that packets cleanly cancel decoding when running out of data, and continue from where they left off
     */
    @Test
    public void testBuffered() throws Exception {
        Random rng = new Random();
        Player player = new Player(rng.nextLong(), "Player", "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg");
        PlayerJoinedPacket encodePacket = new PlayerJoinedPacket(player);
        PlayerJoinedPacket decodePacket = new PlayerJoinedPacket(encodePacket.data, null);
        // decode to the first 10 bytes
        decodePacket.setAvailable(10);
        assertFalse(decodePacket.decode());
        assertNull(decodePacket.getPlayer());
        // add some more (not enough)
        decodePacket.setAvailable(50);
        assertFalse(decodePacket.decode());
        assertNull(decodePacket.getPlayer());
        // decode the whole packet
        decodePacket.setAvailable(Integer.MAX_VALUE);
        assertTrue(decodePacket.decode());
        assertEquals(player.steamId, decodePacket.getPlayer().steamId);
        assertEquals(player.name, decodePacket.getPlayer().name);
        assertEquals(player.iconUrl, decodePacket.getPlayer().iconUrl);
    }
}
