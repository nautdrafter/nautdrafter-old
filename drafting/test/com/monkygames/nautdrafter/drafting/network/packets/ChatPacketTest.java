/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.drafting.network.packets.ChatPacket;
import com.monkygames.nautdrafter.common.network.Player;
import java.util.Base64;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @see com.monkygames.nautdrafter.network.packets.ChatPacket
 */
public class ChatPacketTest {

    public ChatPacketTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encoding and decoding, of class ChatPacket
     */
    @Test
    public void testPacket() throws Exception {
        Random rng = new Random();
        Base64.Encoder stringGen = Base64.getEncoder();
        Player player = new Player(rng.nextLong(), "Player", "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg");
        byte[] buf = new byte[10000];
        rng.nextBytes(buf);
        String message = stringGen.encodeToString(buf);
        ChatPacket encodePacket = new ChatPacket(player, message);
        ChatPacket decodePacket = new ChatPacket(encodePacket.data);
        decodePacket.decode();
        /**
         * packet doesn't contain player name or icon as it is expected the client and server already know these (the
         * player name and icon will be deduced from the player id)
         * <br>
         * TODO: test recovering the full player info once we have the facility for that (ServerConnection class)
         */
        // assertEquals(player.name, decodePacket.getSender().name);
        // assertEquals(player.iconUrl, decodePacket.getSender().iconUrl);
        assertEquals(player.steamId, decodePacket.getSender().steamId);
        assertEquals(message, decodePacket.getMessage());
    }

    /**
     * Test that packets cleanly cancel decoding when running out of data, and continue from where they left off
     */
    @Test
    public void testBuffered() throws Exception {
        Random rng = new Random();
        Base64.Encoder stringGen = Base64.getEncoder();
        Player player = new Player(rng.nextLong(), "Player", "https://gravatar.com/avatar/d56d7460c5b31ce297cfcf6230a6d1a3.jpg");
        byte[] buf = new byte[10000];
        rng.nextBytes(buf);
        String message = stringGen.encodeToString(buf);
        ChatPacket encodePacket = new ChatPacket(player, message);
        ChatPacket decodePacket = new ChatPacket(encodePacket.data);
        // decode to the first 5 bytes (not enough)
        decodePacket.setAvailable(5);
        assertFalse(decodePacket.decode());
        assertNull(decodePacket.getSender());
        assertNull(decodePacket.getMessage());
        // decode the sender only
        decodePacket.setAvailable(100);
        assertFalse(decodePacket.decode());
        assertEquals(player.steamId, decodePacket.getSender().steamId);
        assertNull(decodePacket.getMessage());
        // decode the whole packet
        decodePacket.setAvailable(Integer.MAX_VALUE);
        assertTrue(decodePacket.decode());
        assertEquals(player.steamId, decodePacket.getSender().steamId);
        assertEquals(message, decodePacket.getMessage());
    }
}
