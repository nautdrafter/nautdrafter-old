/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.drafting.network.packets.RoomInfoPacket;
import com.monkygames.nautdrafter.common.network.Player;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.assertEquals;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * @see com.monkygames.nautdrafter.network.packets.RoomInfoPacket
 */
public class RoomInfoPacketTest {

    public RoomInfoPacketTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of encoding and decoding, of class ChatPacket
     */
    @Test
    public void testPacket() throws Exception {
        Random rng = new Random();
        String name = "test room";
        int playerCount = 1 + rng.nextInt(6);
        List<Player> players = new LinkedList<>();
        for (int i = 0; i < playerCount; i++) {
            players.add(new Player(rng.nextLong(), "Player" + rng.nextInt(), "icon" + rng.nextLong()));
        }
        RoomInfoPacket encodePacket = new RoomInfoPacket(name, players);
        RoomInfoPacket decodePacket = new RoomInfoPacket(encodePacket.data);
        decodePacket.decode();
        assertEquals(name, decodePacket.getName());
        Iterator<Player> expected = players.iterator();
        Iterator<Player> actual = decodePacket.getPlayers().iterator();
        while (expected.hasNext()) {
            Player exp = expected.next();
            Player act = actual.next();
            assertEquals(exp.steamId, act.steamId);
            assertEquals(exp.name, act.name);
            assertEquals(exp.iconUrl, act.iconUrl);
        }
    }

}
