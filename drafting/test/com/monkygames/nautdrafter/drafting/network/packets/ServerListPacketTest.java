/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.drafting.network.packets.ServerListPacket;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.Constants;
import java.net.InetAddress;
import java.net.Socket;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * TODO: write documentation for ServerListPacketTest
 */
public class ServerListPacketTest {

    public ServerListPacketTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testServerList() throws Exception {
        Socket s = new Socket(InetAddress.getByName(Constants.LOBBY_ADDRESS), Constants.LOBBY_PORT);
        s.getOutputStream().write(Packet.SERVER_LIST_REQUEST_PACKET_ID);
        byte[] buf = new byte[10000];
        ServerListPacket packet = new ServerListPacket(buf);
        int pos = 0;
        do {
            pos += s.getInputStream().read(buf, pos, 10000 - pos);
        } while (!packet.decode());
        packet.handle();
    }

}
