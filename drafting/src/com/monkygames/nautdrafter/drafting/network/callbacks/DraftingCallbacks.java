/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

/**
 * Handles callbacks specific to the client
 */
public class DraftingCallbacks {

    public static ChatCallback chatCallback;
    public static DraftingRoomCallback draftingRoomCallback;
    public static NautPickRoomCallback nautPickRoomCallback;
    public static PlayerConnectionCallback playerConnectionCallback;
    public static RoomChangeCallback roomChangeCallback;
    public static ServerBrowserCallback serverBrowserCallback;
    public static TeamSelectionRoomCallback teamSelectionRoomCallback;
    public static ClientIDCallback clientIDCallback;
    public static ConnectionCallback connectionCallback;

    private DraftingCallbacks() {
    }
}
