/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

import com.monkygames.nautdrafter.common.network.ServerInfo;
import java.util.List;

/**
 * Used for adding servers to the server browser screen asynchronously from the server thread
 */
public interface ServerBrowserCallback {

    /**
     * Callback for adding some servers to the server browser
     *
     * @param servers The new servers
     */
    public void addServers(List<ServerInfo> servers);

    /**
     * Callback for when the server has finished starting
     *
     * @param success
     */
    public void serverStarted(boolean success);
}
