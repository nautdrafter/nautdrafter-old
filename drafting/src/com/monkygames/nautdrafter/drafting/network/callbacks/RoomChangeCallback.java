/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.drafting.network.Team;
import com.monkygames.nautdrafter.drafting.network.Timeline;
import java.util.List;

/**
 * Used for notifying the main application to change rooms
 */
public interface RoomChangeCallback {

    /**
     * The server wants us to change to the team selection room
     *
     * @param players List of players in the room
     * @param team1 The red team
     * @param team2 The blue team
     * @see TeamSelectionRoomCallback
     */
    public void gotoTeamSelection(List<Player> players, Team team1, Team team2);

    /**
     * The server wants us to change to the drafting room (where the captains of both teams pick/ban Nauts)
     *
     * @param timeline Object specifying the structure of the timeline, and the current position and time
     * @param team1 The red team
     * @param team2 The blue team
     * @see DraftingRoomCallback
     */
    public void gotoDrafting(Timeline timeline, Team team1, Team team2);

    /**
     * The server wants us to change to the Naut Picking room (where players pick a Naut from their team's Nauts)
     *
     * @param team1 The red team
     * @param team2 The blue team
     * @see NautPickRoomCallback
     */
    public void gotoNautPick(Team team1, Team team2);

    /**
     * The server wants us to change to the versus room
     *
     * @param team1 The red team
     * @param team2 The blue team
     */
    public void gotoVersus(Team team1, Team team2);
}
