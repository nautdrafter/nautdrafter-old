/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

import com.monkygames.nautdrafter.common.network.Player;
import java.nio.channels.SelectionKey;

/**
 * Used for notifying rooms of players joining or leaving
 */
public interface PlayerConnectionCallback {

    /**
     * A player has joined the room
     *
     * @param player The (re)connecting player
     * @param key
     */
    public void onPlayerJoin(Player player, SelectionKey key);

    /**
     * A player has left the room
     *
     * @param player The disconnecting player
     * @param key
     */
    public void onPlayerLeave(Player player, SelectionKey key);

}
