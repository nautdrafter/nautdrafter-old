/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.drafting.network.Team;

/**
 * Used for notifying the team selection room of events
 */
public interface TeamSelectionRoomCallback {

    /**
     * A player has left a team, joined a team, or swapped teams
     *
     * @param player The player that changed team
     * @param left The team they left (or null if they didn't leave a team)
     * @param joined The team they joined (or null if they didn't join a team)
     */
    public abstract void onChangedTeam(Player player, Team left, Team joined);
}
