/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

import com.monkygames.nautdrafter.drafting.network.Team;

/**
 * Used for notifying the Naut picking room of events
 */
public interface NautPickRoomCallback {

    /**
     * A player has selected a naut
     *
     * @param team The updated team
     */
    public void onNautSelected(Team team);

    /**
     * A player has deselected a naut (TODO: do we allow this?)
     *
     * @param team The updated team
     */
    public void onNautDeselected(Team team);
}
