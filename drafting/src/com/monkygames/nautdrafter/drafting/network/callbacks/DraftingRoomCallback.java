/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.callbacks;

import com.monkygames.nautdrafter.drafting.network.Team;
import com.monkygames.nautdrafter.drafting.network.Timeline;

/**
 * Used for notifying the drafting room of events
 */
public interface DraftingRoomCallback {

    /**
     * The timeline has updated (a captain has finished their turn)
     *
     * @param timeline The updated timeline (advanced to the next stage)
     * @param updatedTeam The updated team (with the new pick/ban added)
     */
    public void onUpdate(Timeline timeline, Team updatedTeam);

    /**
     * The team captain has selected a naut (not picked/banned yet, just selected)
     *
     * @param selectedIndex The index of the selected naut
     */
    public void onSelect(int selectedIndex);
}
