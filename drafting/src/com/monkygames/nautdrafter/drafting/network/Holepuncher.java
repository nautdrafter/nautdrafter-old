/*
 * Copyright (C) 2014 NautDrafter Drafting
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * TODO: write documentation for Holepuncher
 */
public final class Holepuncher {

    private Holepuncher() {

    }

    /**
     * Attempts to start a connection between Client and Drafting Server <br>
     * TODO: Test this
     *
     * @param addr
     * @param port
     * @param localPort
     */
    public static void connect(InetAddress addr, int port, int localPort, boolean listen) {
        // the Client needs to listen on localPort as well,
        // but the Drafting Server is already listening on localPort
        if (listen) {
            new Thread(() -> {
                try {
                    ServerSocket listener = new ServerSocket();
                    listener.setReuseAddress(true);
                    listener.bind(new InetSocketAddress(localPort));
                    listener.accept();
                    // todo: do something here
                } catch (IOException ex) {
                    Logger.getLogger(Holepuncher.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        }

        Socket sender;
        while (true) {
            sender = new Socket();
            try {
                sender.setReuseAddress(true);
                sender.bind(new InetSocketAddress(localPort));
                sender.connect(new InetSocketAddress(addr, port));
            } catch (IOException ex) {
                Logger.getLogger(Holepuncher.class.getName()).log(Level.SEVERE, null, ex);
                try {
                    sender.close();
                } catch (IOException e1) {
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e1) {
                }
            }
        }
    }
}
