/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network;

import com.monkygames.nautdrafter.common.network.Player;

/**
 * Describes a team
 */
public class Team {

    public final String name;
    public final String iconUrl;
    Player captain, player1, player2;

    public Team(String name, String iconUrl, Player captain, Player player1, Player player2) {
        this.name = name;
        this.iconUrl = iconUrl;
        this.captain = captain;
        this.player1 = player1;
        this.player2 = player2;
    }

    public Player getCaptain() {
        return this.captain;
    }

    public Player[] getPlayers() {
        return new Player[]{captain, player1, player2};
    }
}
