/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.segments.IntSegment;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;
import java.nio.channels.SelectionKey;

/**
 * Player left packet<br>
 * <pre>
 * <b>Element                  Length (bytes)</b>
 * PLAYER_LEFT_PACKET_ID    1
 * PLAYER_ID                8
 * </pre>
 */
public class PlayerLeftPacket extends Packet {

    public static final byte PACKET_ID = Packet.PLAYER_LEFT_PACKET_ID;

    private static final IntSegment playerSegment = new IntSegment(8);

    private Long playerId = null;
    private Player player;
    private SelectionKey key;

    public PlayerLeftPacket(byte[] data, SelectionKey key) throws PacketDecoderException {
        super(data);
        this.key = key;
    }

    public PlayerLeftPacket(Player player) throws PacketEncoderException {
        this.player = player;

        byte[] idBuffer = playerSegment.encode(this.player.steamId);
        this.data = new byte[1 + idBuffer.length];
        this.data[0] = PACKET_ID;
        System.arraycopy(idBuffer, 0, data, 1, idBuffer.length);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        // player id
        if (this.playerId == null) {
            if (!playerSegment.can_decode(this)) {
                return false;
            }
            this.playerId = playerSegment.decode(this);
            this.decode_from = this.pos;
        }
        // player
        // this.player = ServerConnection.instance().players.get(playerId);
        this.player = new Player(playerId, "name", "icon");

        return true;
    }

    /**
     * @return The player
     */
    public Player getPlayer() {
        return this.player;
    }

    @Override
    protected void handle_impl() {
        if (DraftingCallbacks.playerConnectionCallback != null) {
            DraftingCallbacks.playerConnectionCallback.onPlayerLeave(this.player, this.key);
        }
    }
}
