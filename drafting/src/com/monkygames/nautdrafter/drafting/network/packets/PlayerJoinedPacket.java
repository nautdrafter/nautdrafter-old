/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.UserDetailsPacket;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;
import java.nio.channels.SelectionKey;

/**
 * Player joined packet
 */
public class PlayerJoinedPacket extends UserDetailsPacket {

    private SelectionKey key;

    public PlayerJoinedPacket(byte[] data, SelectionKey key) throws PacketDecoderException {
        super(data);
        this.key = key;
    }

    public PlayerJoinedPacket(Player player) throws PacketEncoderException {
        super(player, Packet.PLAYER_JOINED_PACKET_ID);
    }

    @Override
    protected void handle_impl() {
        if (DraftingCallbacks.playerConnectionCallback != null) {
            DraftingCallbacks.playerConnectionCallback.onPlayerJoin(this.player, this.key);
        }
    }
}
