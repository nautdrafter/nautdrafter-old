/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.segments.PlayerSegment;
import com.monkygames.nautdrafter.common.network.packets.segments.StringSegment;
import java.util.LinkedList;
import java.util.List;

/**
 * Room info packet<br>
 * <pre>
 * <b>Element                  Length (bytes)</b>
 * ROOM_INFO_PACKET_ID      1
 * NAME_LENGTH              1
 * PLAYER_COUNT             1
 * NAME                     NAME_LENGTH
 * PLAYERS                  PLAYER_COUNT * PLAYER_JOINED_PACKET
 * </pre>
 */
public class RoomInfoPacket extends Packet {

    public static final byte PACKET_ID = Packet.ROOM_INFO_PACKET_ID;

    private static final StringSegment nameSegment = new StringSegment(1, true);
    private static final PlayerSegment playerSegment = new PlayerSegment();

    private String name = null;
    private List<Player> players = null;

    private Integer playerCount = null;

    public RoomInfoPacket(byte[] data) throws PacketDecoderException {
        super(data);
    }

    public RoomInfoPacket(String name, List<Player> players) throws PacketEncoderException {
        this.name = name;
        this.players = players;

        byte[] nameBuffer = nameSegment.encode(this.name);
        byte playerCount = (byte) (this.players.size() & 0xFF);
        byte[][] playerBuffer = new byte[playerCount][];
        int i = 0;
        int playersLength = 0;
        for (Player player : this.players) {
            playerBuffer[i] = playerSegment.encode(player);
            playersLength += playerBuffer[i].length;
            i++;
        }
        this.data = new byte[1 + nameBuffer.length + 1 + playersLength];
        this.data[0] = PACKET_ID;
        System.arraycopy(nameBuffer, 0, this.data, 1, nameBuffer.length);
        this.data[nameBuffer.length + 1] = playerCount;
        int p = nameBuffer.length + 2;
        for (byte[] player : playerBuffer) {
            System.arraycopy(player, 0, this.data, p, player.length);
            p += player.length;
        }
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        // name
        if (this.name == null) {
            if (!nameSegment.can_decode(this)) {
                return false;
            }
            this.name = nameSegment.decode(this);
            this.decode_from = this.pos;
        }
        // player count
        if (this.playerCount == null) {
            if (this.available() < 1) {
                return false;
            }
            this.playerCount = (this.data[this.pos++] & 0xFF);
            this.decode_from = this.pos;
        }
        // players
        if (this.players == null) {
            this.players = new LinkedList<>();
        }
        for (; this.playerCount > 0; this.playerCount--) {
            if (!PlayerSegment.can_decode(this)) {
                return false;
            }
            this.players.add(playerSegment.decode(this));
            this.decode_from = this.pos;
        }

        return true;
    }

    public String getName() {
        return this.name;
    }

    public List<Player> getPlayers() {
        return this.players;
    }

    @Override
    protected void handle_impl() {
        // TODO
    }

}
