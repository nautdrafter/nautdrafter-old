/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.common.network.packets.AbstractClientIDPacket;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;

/**
 * The packet that will contain just the ClientID
 */
public class ClientIdPacket extends AbstractClientIDPacket {
    /**
     * The constructor to create a packet with the ClientID inside it in byte[] form
     * @param data 
     */
    public ClientIdPacket(byte[] data) {
        super(data);
    }
    
    /**
     * The constructor to create a packet with the ClientID inside it in String form
     * @param clientid
     * @throws PacketEncoderException 
     */
    public ClientIdPacket(String clientid) throws PacketEncoderException {
        super(clientid);
    }
    
    /**
     * The callback to call when this packet needs to be handled
     */
    @Override
    protected void handle_impl() {
        if (DraftingCallbacks.clientIDCallback != null) {
            DraftingCallbacks.clientIDCallback.onClientIDReceived(this.clientid);
        }
    }
}
