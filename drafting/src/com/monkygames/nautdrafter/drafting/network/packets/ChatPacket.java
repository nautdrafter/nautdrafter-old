/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network.packets;

import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketDecoderException;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.segments.IntSegment;
import com.monkygames.nautdrafter.common.network.packets.segments.StringSegment;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;

/**
 * Chat message packet<br>
 * <pre>
 * <b>Element                  Length (bytes)</b>
 * CHAT_PACKET_ID           1
 * PLAYER_ID                8
 * MESSAGE_LENGTH           2
 * MESSAGE                  MESSAGE_LENGTH
 * </pre>
 */
public class ChatPacket extends Packet {

    public static final byte PACKET_ID = Packet.CHAT_PACKET_ID;

    private static final IntSegment senderSegment = new IntSegment(8);
    private static final StringSegment messageSegment = new StringSegment(2, true);

    private Long playerId = null;
    private Player sender = null;
    private String message = null;

    public ChatPacket(byte[] data) throws PacketDecoderException {
        super(data);
    }

    public ChatPacket(Player sender, String message) throws PacketEncoderException {
        this.sender = sender;
        this.message = message;

        byte[] idBuffer = senderSegment.encode(this.sender.steamId);
        byte[] msgBuffer = messageSegment.encode(this.message);
        this.data = new byte[1 + idBuffer.length + msgBuffer.length];
        this.data[0] = PACKET_ID;
        System.arraycopy(idBuffer, 0, this.data, 1, idBuffer.length);
        System.arraycopy(msgBuffer, 0, this.data, 1 + idBuffer.length, msgBuffer.length);
    }

    @Override
    protected boolean decode_impl() throws PacketDecoderException {
        // player id
        if (this.playerId == null) {
            if (!senderSegment.can_decode(this)) {
                return false;
            }
            this.playerId = senderSegment.decode(this);
            this.decode_from = this.pos;
        }
        // player
        // this.sender = ServerConnection.instance().players.get(playerId);
        this.sender = new Player(playerId, "name", "icon");
        // message
        if (this.message == null) {
            if (!messageSegment.can_decode(this)) {
                return false;
            }
            this.message = messageSegment.decode(this);
            this.decode_from = this.pos;
        }

        return true;
    }

    /**
     * @return The player who sent the message
     */
    public Player getSender() {
        return this.sender;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return this.message;
    }

    @Override
    protected void handle_impl() {
        if (DraftingCallbacks.chatCallback != null) {
            DraftingCallbacks.chatCallback.onChatReceived(this.sender, this.message);
        }
    }
}
