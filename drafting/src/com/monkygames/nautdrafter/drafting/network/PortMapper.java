/*
 * Copyright (C) 2014 NautDrafter Drafting
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network;

import java.io.IOException;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.ParserConfigurationException;
import org.bitlet.weupnp.GatewayDevice;
import org.bitlet.weupnp.GatewayDiscover;
import org.xml.sax.SAXException;

/**
 * TODO: write documentation for PortMapper
 */
public class PortMapper {

    private PortMapper() {

    }

    public static boolean map(int localPort) {
        Logger logger = Logger.getLogger(PortMapper.class.getName());
        try {
            logger.info("Starting weupnp");

            GatewayDiscover discover = new GatewayDiscover();
            logger.info("Looking for Gateway Devices");
            discover.discover();

            GatewayDevice gateway = discover.getValidGateway();

            if (gateway != null) {

                logger.log(Level.INFO, "Found gateway device.\n{0} ({1})",
                        new Object[]{gateway.getModelName(), gateway.getModelDescription()});

                InetAddress localAddress = gateway.getLocalAddress();
                logger.log(Level.INFO, "Using local address: {0}", localAddress);
                String externalIPAddress = gateway.getExternalIPAddress();
                logger.log(Level.INFO, "External address: {0}", externalIPAddress);

                logger.log(Level.INFO, "Attempting to map port {0}", localPort);

                // try to assign a port
                int externalPort = localPort;
                int delta = 1;
                boolean success = false;
                for (int i = 0; i < 10; i++) {
                    if (gateway.addPortMapping(externalPort, localPort, localAddress.getHostAddress(), "TCP", "NautDrafter")) {
                        success = true;
                        break;
                    }
                    externalPort += delta;
                    if (externalPort >= 0xFFFF || externalPort <= 1024) {
                        delta *= -1;
                        externalPort = localPort + delta;
                    }
                }

                if (success) {
                    logger.log(Level.INFO, "Mapping successful!");
                    return true;
                } else {
                    logger.severe("Port mapping attempt failed");
                }
            } else {
                logger.info("No valid gateway device found.");
            }
        } catch (IOException | SAXException | ParserConfigurationException ex) {
            logger.log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
