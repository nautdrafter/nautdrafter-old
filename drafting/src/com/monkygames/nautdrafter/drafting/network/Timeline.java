/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network;

import java.util.ArrayList;

/**
 * Describes the structure of the Timeline for a drafting room
 */
public class Timeline {

    public static enum Team {
        RED, BLUE
    }
    
    public static enum Type {
        PICK, BAN
    }
    
    public static class TimelineItem {
        public final Team direction;
        public final Type type;
        public final int time;
        
        public TimelineItem(Team team, Type type, int time) {
            this.direction = team;
            this.type = type;
            this.time = time;
        }
    }

    private final ArrayList<TimelineItem> items;

    public Timeline() {
        this.items = new ArrayList(12);
    }
}
