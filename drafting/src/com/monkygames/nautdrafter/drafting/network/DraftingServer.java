/*
 * Copyright (C) 2014 NautDrafter
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.monkygames.nautdrafter.drafting.network;

import com.monkygames.nautdrafter.common.Constants;
import com.monkygames.nautdrafter.common.network.Player;
import com.monkygames.nautdrafter.common.network.Server;
import com.monkygames.nautdrafter.common.network.ServerInfo;
import com.monkygames.nautdrafter.common.network.packets.Packet;
import com.monkygames.nautdrafter.common.network.packets.PacketEncoderException;
import com.monkygames.nautdrafter.common.network.packets.ServerAnnouncePacketEncoder;
import com.monkygames.nautdrafter.common.network.packets.ServerUpdatePacketEncoder;
import com.monkygames.nautdrafter.drafting.network.callbacks.DraftingCallbacks;
import com.monkygames.nautdrafter.drafting.network.callbacks.PlayerConnectionCallback;
import com.monkygames.nautdrafter.drafting.network.packets.ChatPacket;
import com.monkygames.nautdrafter.drafting.network.packets.PlayerJoinedPacket;
import com.monkygames.nautdrafter.drafting.network.packets.PlayerLeftPacket;
import com.sun.javafx.application.ParametersImpl;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.util.Pair;

/**
 * Drafting Server
 */
public class DraftingServer extends Server implements PlayerConnectionCallback {

    private final SelectionKey lobbyKey;
    private final SocketChannel lobbyChannel;
    private final String name, description, password;

    private final Map<Pair<InetAddress, Integer>, Player> players = new HashMap<>();

    public DraftingServer(String name, String description, String password, boolean upnp) throws IOException {
        super(null);

        if (upnp) {
            PortMapper.map(this.ssc.socket().getLocalPort());
        }

        this.name = name;
        this.description = description;
        this.password = password;
        this.lobbyChannel = SocketChannel.open();
        this.lobbyChannel.connect(new InetSocketAddress(Constants.LOBBY_ADDRESS, Constants.LOBBY_PORT));
        this.lobbyChannel.configureBlocking(false);
        ConnectionInfo info = new ConnectionInfo();
        this.lobbyChannel.finishConnect();
        Logger.getLogger(DraftingServer.class.getName()).log(Level.INFO, "Connected to Lobby server at {0}:{1}", new Object[]{lobbyChannel.socket().getInetAddress().getHostAddress(), lobbyChannel.socket().getPort()});
        this.lobbyKey = this.lobbyChannel.register(this.selector, SelectionKey.OP_READ, info);
        // announce ourselves as a server
        info.send_buffer.put(
                new ServerAnnouncePacketEncoder(
                        new ServerInfo(
                                this.name,
                                this.description,
                                null,
                                0,
                                this.getPort(),
                                0,
                                this.password != null
                        )
                ).data
        );
        this.write(this.lobbyKey);

        DraftingCallbacks.playerConnectionCallback = this;
    }

    @Override
    protected void kill(SelectionKey key) {
        Socket s = ((SocketChannel) key.channel()).socket();
        System.out.println("Killing connection from " + s.getInetAddress() + ":" + s.getPort());
        super.kill(key);
        if (this.players.remove(new Pair(s.getInetAddress(), s.getPort())) != null) {
            this.sendUpdate();
        }
        if ((!this.lobbyKey.isValid() || !this.lobbyChannel.isConnected()) && this.players.size() <= 0) {
            this.shutdown();
        }
    }

    @Override
    protected Packet createPacket(SelectionKey key, byte[] data) throws IOException {
        switch (data[0]) {
        case Packet.CHAT_PACKET_ID:
            return new ChatPacket(data);
        case Packet.PLAYER_JOINED_PACKET_ID:
            return new PlayerJoinedPacket(data, key);
        case Packet.PLAYER_LEFT_PACKET_ID:
            return new PlayerLeftPacket(data, key);
        }
        return null;
    }

    private void sendUpdate() {
        try {
            ConnectionInfo info = (ConnectionInfo) this.lobbyKey.attachment();
            info.send_buffer.put(new ServerUpdatePacketEncoder(this.players.size()).data);
            this.write(this.lobbyKey);
        } catch (PacketEncoderException ex) {
            Logger.getLogger(DraftingServer.class.getName()).log(Level.SEVERE, null, ex);
            this.kill(this.lobbyKey);
        }
    }

    @Override
    public void onPlayerJoin(Player player, SelectionKey key) {
        Socket s = ((SocketChannel) key.channel()).socket();
        this.players.put(new Pair(s.getInetAddress(), s.getPort()), player);
        Logger.getLogger(DraftingServer.class.getName()).log(Level.INFO, "{0} joined", player.name);
        this.sendUpdate();
    }

    @Override
    public void onPlayerLeave(Player player, SelectionKey key) {
        Socket s = ((SocketChannel) key.channel()).socket();
        if (this.players.remove(new Pair(s.getInetAddress(), s.getPort())) != null) {
            Logger.getLogger(DraftingServer.class.getName()).log(Level.INFO, "{0} left", player.name);
            this.sendUpdate();
        }
    }

    public static void main(String[] args) throws IOException {

        Application.Parameters parameters = new ParametersImpl(args);

        Map<String, String> named = parameters.getNamed();

        try {
            String name = named.getOrDefault("name", "name");
            String description = named.getOrDefault("description", "");
            String password = named.get("pass");
            boolean upnp = Boolean.parseBoolean(named.getOrDefault("upnp", "true"));

            new DraftingServer(name, description, password, upnp).run();

        } catch (IOException e) {
            System.err.println("Usage: java -jar DraftingServer.jar <args>");
            System.err.println("  --name=String           sets the name of the server");
            System.err.println("  --description=String    sets the description of the server");
            System.err.println("  --pass=String           sets the password of the server");
            System.err.println("  --upnp=Boolean        disables upnp (defaults to enabled)");
        }
    }
}
